<?php

namespace app\models;

use app\components\BaseActiveRecord;
use app\components\services\CacheService;
use app\components\services\FileService;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "file".
 *
 * @property string $id
 * @property string $name
 * @property string $filename
 * @property string $url
 * @property string $extension
 * @property string $status_id
 * @property string $created
 * @property string $updated
 * @property string $deleted
 * @property integer $created_by
 * @property integer $updated_by
 *
 *
 * @property $filePath
 * @property NewsArticleFile[] $newsArticleFiles
 * @property AdvertisementFile[] $advertisementFiles
 * @property PageFile[] $pageFiles
 */
class File extends BaseActiveRecord
{
    public function init()
    {
        parent::init();

        $this->on(self::AFTER_SOFT_DELETE, function() {

            // При удалении файла, удаляем связи из таблицы {{%news_article_file}}
            $newsArticleFiles = $this->newsArticleFiles;
            foreach ($newsArticleFiles as $newsArticleFile) {
                $newsArticleFile->delete();
            }

            // При удалении файла, удаляем связи из таблицы {{%apartment_advertisement_file}}
            $advertisementFiles = $this->advertisementFiles;
            foreach ($advertisementFiles as $advertisementFile) {
                $advertisementFile->delete();
            }

            // При удалении файла, удаляем связи из таблицы {{%page_file}}
            $pageFiles = $this->pageFiles;
            foreach ($pageFiles as $pageFile) {
                $pageFile->delete();
            }
        });
    }

    public static function tableName()
    {
        return 'file';
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'name',
            'extension',
            'filename',
            'path' => function() {
                return $this->filePath;
            }
        ]);
    }

    public function rules()
    {
        return [
            ['name', 'trim'],
            ['name', 'string', 'max' => 255],

            ['filename', 'trim'],
            ['filename', 'string', 'max' => 255],

            ['url', 'trim'],
            ['url', 'string'],

            ['extension', 'trim'],
            ['extension', 'string', 'max' => 10],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    public function getNewsArticleFiles()
    {
        return $this->hasMany(NewsArticleFile::className(), ['file_id' => 'id']);
    }

    public function getAdvertisementFiles()
    {
        return $this->hasMany(AdvertisementFile::className(), ['file_id' => 'id']);
    }

    public function getPageFiles()
    {
        return $this->hasMany(PageFile::className(), ['file_id' => 'id']);
    }

    public function getFilePath()
    {
        return FileService::getFilePath($this);
    }

    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}
