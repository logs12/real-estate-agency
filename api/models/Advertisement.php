<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;
use app\components\behaviors\UploadFileBehavior;
use app\components\services\CacheService;


/**
 * This is the model class for table "advertisement".
 *
 * @property int $id Identifier
 * @property int $internal_id Идентификатор объявления из импортируемого xml документа
 * @property int $is_manually_added true-добавлено вручную, false - импортированно
 * @property string $title Название объявления
 * @property string $country Страна
 * @property string $region Область
 * @property string $city Город
 * @property string $sub_locality_name Район
 * @property string $address Аддрес квартиры
 * @property string $price Цена
 * @property int $currency_id Идентификатор валюты
 * @property int $is_mortgage true-есть ипотека, false - нет ипотеки
 * @property int $count_rooms Количество комнат
 * @property int $number_floor Номер этажа
 * @property int $floors_total Общее количество этажей в доме
 * @property int $is_new_building true - новостройка, false - вторичка
 * @property int $is_best_offer true - являеться лучшим предложением, false - не являеться
 * @property int $is_phone true-есть телефон, false - нет телефона
 * @property int $is_internet true-есть интернет, false - нет интеренета
 * @property int $is_parking true-есть паркинг, false - нет паркинга
 * @property int $is_rubbish_chute true-есть мусоропровод, false - нет мусоропровода
 * @property int $is_lift true-есть лифт, false - нет лифт
 * @property string $description Описание
 * @property string $building_type Тип дома
 * @property string $building_series Серия дома
 * @property int $total_area Общая площадь
 * @property int $lot_area Площадь участка
 * @property int $kitchen_space Площадь кухни
 * @property int $living_space Жилая площадь
 * @property int $file_id Идентификатор файла фотографии
 * @property int $status_id Identifier status
 * @property string $discriminator Дискриминатор сущности
 * @property int $created Date created
 * @property int $updated Date updated
 * @property int $deleted Date deleted
 *
 * @property Currency $currency
 * @property File $file
 * @property Status $status
 * @property AdvertisementFile[] $advertisementFiles
 */
class Advertisement extends BaseActiveRecord
{

    const APARTMENT_ADVERTISEMENT_DISCRIMINATOR = 'apartment';
    const COTTAGE_HOUSES_ADVERTISEMENT_DISCRIMINATOR = 'commercial_estate';
    const COMMERCIAL_ESTATE_ADVERTISEMENT_DISCRIMINATOR = 'cottage_houses_land';

    public $photos;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%advertisement}}';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'uploadFile' => [
                'class' => UploadFileBehavior::className(),
                'relationModelAttributes' => ['photos' => [AdvertisementFile::className() => ['file_id', 'advertisement_id']]],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['is_manually_added'], 'integer'],

            [['internal_id'], 'integer'],

            [['title'], 'required'],
            [['title' ], 'string', 'max' => 155],

            [['count_rooms'], 'integer'],
            [['count_rooms'], 'required'],

            [['number_floor'], 'integer'],
            [['number_floor'], 'required'],

            [['floors_total'], 'integer'],
            [['floors_total'], 'required'],

            [['is_new_building'], 'integer'],

            [['is_phone'], 'integer'],

            [['is_mortgage'], 'integer'],

            [['is_best_offer'], 'integer'],

            [['is_internet'], 'integer'],

            [['is_parking'], 'integer'],

            [['is_rubbish_chute'], 'integer'],

            [['is_lift'], 'integer'],

            [['total_area'], 'integer'],

            [['kitchen_space'], 'integer'],
            [['kitchen_space'], 'required'],

            [['living_space'], 'integer'],
            [['living_space'], 'required'],

            [['total_area'], 'integer'],
            [['total_area'], 'required'],

            [['lot_area'], 'integer'],

            [['price'], 'integer'],
            [['price'], 'required'],

            [[
                'country',
                'region',
                'city',
                'sub_locality_name',
                'address',
                'building_type',
                'building_series'
            ], 'string', 'max' => 155],


            [['total_area'], 'required'],
            [['description'], 'string'],

            [['discriminator'], 'required'],
            [['discriminator'], 'string'],

            ['currency_id', 'required'],
            ['currency_id', 'integer'],
            [['currency_id'], 'exist', 'skipOnError' => true, 'targetClass' => Currency::className(), 'targetAttribute' => ['currency_id' => 'id']],

            ['photos', 'file', 'maxFiles' => 50/*, 'extensions' => 'png, jpg, text/html'*/],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],

        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/advertisement', 'id'),
            'is_manually_added' => Yii::t('app/advertisement', 'is_manually_added'),
            'country' => Yii::t('app/advertisement', 'country'),
            'title' => Yii::t('app/advertisement', 'title'),
            'region' => Yii::t('app/advertisement', 'region'),
            'city' => Yii::t('app/advertisement', 'city'),
            'sub_locality_name' => Yii::t('app/advertisement', 'sub_locality_name'),
            'address' => Yii::t('app/advertisement', 'address'),
            'price' => Yii::t('app/advertisement', 'price'),
            'currency' => Yii::t('app/advertisement', 'currency_id'),
            'count_rooms' => Yii::t('app/advertisement', 'count_rooms'),
            'number_floor' => Yii::t('app/advertisement', 'number_floor'),
            'floors_total' => Yii::t('app/advertisement', 'floors_total'),
            'is_new_building' => Yii::t('app/advertisement', 'is_new_building'),
            'is_best_offer' => Yii::t('app/advertisement', 'is_best_offer'),
            'is_phone' => Yii::t('app/advertisement', 'is_phone'),
            'is_internet' => Yii::t('app/advertisement', 'is_internet'),
            'is_parking' => Yii::t('app/advertisement', 'is_parking'),
            'is_rubbish_chute' => Yii::t('app/advertisement', 'is_rubbish_chute'),
            'is_lift' => Yii::t('app/advertisement', 'is_lift'),
            'description' => Yii::t('app/advertisement', 'description'),
            'building_type' => Yii::t('app/advertisement', 'building_type'),
            'building_series' => Yii::t('app/advertisement', 'building_series'),
            'total_area' => Yii::t('app/advertisement', 'total_area'),
            'lot_area' => Yii::t('app/advertisement', 'lot_area'),
            'kitchen_space' => Yii::t('app/advertisement', 'kitchen_space'),
            'living_space' => Yii::t('app/advertisement', 'living_space'),
            'photos' => Yii::t('app/advertisement', 'photos'),
            'status' => Yii::t('app/advertisement', 'status'),
            'created' => Yii::t('app/advertisement', 'created'),
            'updated' => Yii::t('app/advertisement', 'updated'),
            'deleted' => Yii::t('app/advertisement', 'deleted'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'is_manually_added',
            'country',
            'region',
            'city',
            'sub_locality_name',
            'address',
            'price',
            'currency' => function($model) {
                return $model->currency->abbreviation;
            },
            'count_rooms',
            'number_floor',
            'floors_total',
            'is_new_building',
            'is_best_offer',
            'is_phone',
            'is_internet',
            'is_parking',
            'is_rubbish_chute',
            'is_lift',
            'description',
            'building_type',
            'building_series',
            'total_area',
            'lot_area',
            'kitchen_space',
            'living_space',
            'discriminator',
            'status' => function($model) {
                return $model->status;
            },
            'created',
            'updated',
            'photos' => function($model) {
                return $model->advertisementFiles;
            },
            'title',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCurrency()
    {
        return $this->hasOne(Currency::className(), ['id' => 'currency_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisementFiles()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])
            ->viaTable('advertisement_file', ['advertisement_id' => 'id']);
    }
}
