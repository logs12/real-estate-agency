<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "page_file".
 *
 * @property int $id Identifier record
 * @property int $page_id Иднтификатор страницы
 * @property int $file_id Identifier file
 *
 * @property Page $page
 * @property File $file
 */
class PageFile extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['page_id'], 'integer'],

            ['page_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Advertisement::className(),
                'targetAttribute' => ['page_id' => 'id']
            ],

            [['file_id'], 'integer'],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'page_id' => 'Page ID',
            'file_id' => 'File ID',
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'page_id' => function ($model) {
                return $this->getIntOrNull($model->page_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getPage()
    {
        return $this->hasOne(Page::className(), ['id' => 'page_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}