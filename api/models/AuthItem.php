<?php

namespace app\models;

use app\components\BaseActiveRecord;
use app\components\behaviors\AuthItemBehavior;
use yii\helpers\ArrayHelper;
use yii\behaviors\SluggableBehavior;

class AuthItem extends BaseActiveRecord{

    //========== ROLES ==========//
    const ROLE_ROOT = 'ROLE_ROOT';
    const ROLE_ADMIN = 'ROLE_ADMIN';
    const ROLE_USER = 'ROLE_USER';
    //========== /ROLES ==========//

    //========== PERMISSIONS ==========//

    // Роли
    const PERMISSION_ROLE_GET = 'PERMISSION_ROLE_GET';
    const PERMISSION_ROLE_CREATE = 'PERMISSION_ROLE_CREATE';
    const PERMISSION_ROLE_UPDATE = 'PERMISSION_ROLE_UPDATE';
    const PERMISSION_ROLE_DELETE = 'PERMISSION_ROLE_DELETE';

    // Настройки
    const PERMISSION_CONFIG_GET = 'PERMISSION_CONFIG_GET';
    const PERMISSION_CONFIG_CREATE = 'PERMISSION_CONFIG_CREATE';
    const PERMISSION_CONFIG_UPDATE = 'PERMISSION_CONFIG_UPDATE';
    const PERMISSION_CONFIG_DELETE = 'PERMISSION_CONFIG_DELETE';

    // Файлы
    const PERMISSION_FILE_GET = 'PERMISSION_FILE_GET';
    const PERMISSION_FILE_CREATE = 'PERMISSION_FILE_CREATE';
    const PERMISSION_FILE_UPDATE = 'PERMISSION_FILE_UPDATE';
    const PERMISSION_FILE_DELETE = 'PERMISSION_FILE_DELETE';

    // Статусы
    const PERMISSION_STATUS_GET = 'PERMISSION_STATUS_GET';
    const PERMISSION_STATUS_CREATE = 'PERMISSION_STATUS_CREATE';
    const PERMISSION_STATUS_UPDATE = 'PERMISSION_STATUS_UPDATE';
    const PERMISSION_STATUS_DELETE = 'PERMISSION_STATUS_DELETE';

    // Пользователи
    const PERMISSION_USER_GET = 'PERMISSION_USER_GET';
    const PERMISSION_USER_CREATE = 'PERMISSION_USER_CREATE';
    const PERMISSION_USER_UPDATE = 'PERMISSION_USER_UPDATE';
    const PERMISSION_USER_DELETE = 'PERMISSION_USER_DELETE';

    // Разрешения
    const PERMISSION_PERMISSION_GET = 'PERMISSION_PERMISSION_GET';
    const PERMISSION_PERMISSION_CREATE = 'PERMISSION_PERMISSION_CREATE';
    const PERMISSION_PERMISSION_UPDATE = 'PERMISSION_PERMISSION_UPDATE';
    const PERMISSION_PERMISSION_DELETE = 'PERMISSION_PERMISSION_DELETE';

    // Building
    const PERMISSION_BUILDING_GET = 'PERMISSION_BUILDING_GET';
    const PERMISSION_BUILDING_CREATE = 'PERMISSION_BUILDING_CREATE';
    const PERMISSION_BUILDING_UPDATE = 'PERMISSION_BUILDING_UPDATE';
    const PERMISSION_BUILDING_DELETE = 'PERMISSION_BUILDING_DELETE';

    // Floor
    const PERMISSION_FLOOR_GET = 'PERMISSION_FLOOR_GET';
    const PERMISSION_FLOOR_CREATE = 'PERMISSION_FLOOR_CREATE';
    const PERMISSION_FLOOR_UPDATE = 'PERMISSION_FLOOR_UPDATE';
    const PERMISSION_FLOOR_DELETE = 'PERMISSION_FLOOR_DELETE';

    // Flat
    const PERMISSION_FLAT_GET = 'PERMISSION_FLAT_GET';
    const PERMISSION_FLAT_CREATE = 'PERMISSION_FLAT_CREATE';
    const PERMISSION_FLAT_UPDATE = 'PERMISSION_FLAT_UPDATE';
    const PERMISSION_FLAT_DELETE = 'PERMISSION_FLAT_DELETE';

    // Room
    const PERMISSION_ROOM_GET = 'PERMISSION_ROOM_GET';
    const PERMISSION_ROOM_CREATE = 'PERMISSION_ROOM_CREATE';
    const PERMISSION_ROOM_UPDATE = 'PERMISSION_ROOM_UPDATE';
    const PERMISSION_ROOM_DELETE = 'PERMISSION_ROOM_DELETE';

    // NewsArticle
    const PERMISSION_NEWS_ARTICLE_GET = 'PERMISSION_NEWS_ARTICLE_GET';
    const PERMISSION_NEWS_ARTICLE_CREATE = 'PERMISSION_NEWS_ARTICLE_CREATE';
    const PERMISSION_NEWS_ARTICLE_UPDATE = 'PERMISSION_NEWS_ARTICLE_UPDATE';
    const PERMISSION_NEWS_ARTICLE_DELETE = 'PERMISSION_NEWS_ARTICLE_DELETE';

    // ApartmentAdvertisement
    const PERMISSION_APARTMENT_ADVERTISEMENT_GET = 'PERMISSION_APARTMENT_ADVERTISEMENT_GET';
    const PERMISSION_APARTMENT_ADVERTISEMENT_CREATE = 'PERMISSION_APARTMENT_ADVERTISEMENT_CREATE';
    const PERMISSION_APARTMENT_ADVERTISEMENT_UPDATE = 'PERMISSION_APARTMENT_ADVERTISEMENT_UPDATE';
    const PERMISSION_APARTMENT_ADVERTISEMENT_DELETE = 'PERMISSION_APARTMENT_ADVERTISEMENT_DELETE';

    // HomesteadAdvertisement
    const PERMISSION_HOMESTEAD_ADVERTISEMENT_GET = 'PERMISSION_HOMESTEAD_ADVERTISEMENT_GET';
    const PERMISSION_HOMESTEAD_ADVERTISEMENT_CREATE = 'PERMISSION_HOMESTEAD_ADVERTISEMENT_CREATE';
    const PERMISSION_HOMESTEAD_ADVERTISEMENT_UPDATE = 'PERMISSION_HOMESTEAD_ADVERTISEMENT_UPDATE';
    const PERMISSION_HOMESTEAD_ADVERTISEMENT_DELETE = 'PERMISSION_HOMESTEAD_ADVERTISEMENT_DELETE';

    // TradingEstateAdvertisement
    const PERMISSION_TRADING_ESTATE_ADVERTISEMENT_GET = 'PERMISSION_TRADING_ESTATE_ADVERTISEMENT_GET';
    const PERMISSION_TRADING_ESTATE_ADVERTISEMENT_CREATE = 'PERMISSION_TRADING_ESTATE_ADVERTISEMENT_CREATE';
    const PERMISSION_TRADING_ESTATE_ADVERTISEMENT_UPDATE = 'PERMISSION_TRADING_ESTATE_ADVERTISEMENT_UPDATE';
    const PERMISSION_TRADING_ESTATE_ADVERTISEMENT_DELETE = 'PERMISSION_TRADING_ESTATE_ADVERTISEMENT_DELETE';

    // CustomerRequest
    const PERMISSION_CUSTOMER_REQUEST_GET = 'PERMISSION_CUSTOMER_REQUEST_GET';
    const PERMISSION_CUSTOMER_REQUEST_CREATE = 'PERMISSION_CUSTOMER_REQUEST_CREATE';
    const PERMISSION_CUSTOMER_REQUEST_UPDATE = 'PERMISSION_CUSTOMER_REQUEST_UPDATE';
    const PERMISSION_CUSTOMER_REQUEST_DELETE = 'PERMISSION_CUSTOMER_REQUEST_DELETE';

    // Отчёты импорта
    const PERMISSION_REPORT_IMPORT_GET = 'PERMISSION_REPORT_IMPORT_GET';
    const PERMISSION_REPORT_IMPORT_VIEW = 'PERMISSION_REPORT_IMPORT_VIEW';
    const PERMISSION_REPORT_IMPORT_CREATE = 'PERMISSION_REPORT_IMPORT_CREATE';
    const PERMISSION_REPORT_IMPORT_UPDATE = 'PERMISSION_REPORT_IMPORT_UPDATE';
    const PERMISSION_REPORT_IMPORT_DELETE = 'PERMISSION_REPORT_IMPORT_DELETE';

    // Страницы
    const PAGE_GET = 'PAGE_GET';
    const PAGE_VIEW = 'PAGE_VIEW';
    const PAGE_CREATE = 'PAGE_CREATE';
    const PAGE_UPDATE = 'PAGE_CREATE';
    const PAGE_DELETE = 'PAGE_CREATE';

    //========== /PERMISSIONS ==========//

    public $permissions = [];

    public static function tableName()
    {
        return 'auth_item';
    }

    public static function primaryKey()
    {
        return ['id'];
    }

    public function behaviors()
    {
        return ArrayHelper::merge(
            parent::behaviors(),
            [
                [
                    'class' => SluggableBehavior::className(),
                    'attribute' => 'description',
                    'slugAttribute' => 'name',
                ],
                'authItemBehavior' => AuthItemBehavior::className(),
            ]
        );
    }

    public function rules()
    {
        return [
            ['description', 'required'],
            ['description', 'unique', 'targetAttribute' => ['description', 'contractor_id']],
            ['description', 'string', 'max' => 50],

            ['name', 'trim'],
            ['name', 'required'],
            ['name', 'unique', 'targetAttribute' => ['name', 'contractor_id']],
            ['name', 'string', 'max' => 64],

            ['contractor_id', 'required', 'when' => function () {
                return ! \Yii::$app->user->identity->isAdmin();
            }],
        ];
    }

    public function attributeLabels()
    {
        return [
            'description' => 'Название',
        ];
    }

    public function fields()
    {
        $fields = ArrayHelper::merge(parent::fields(), [
            'name',
            'description',
        ]);

        // Если это роль - прибавление к результату разрешений этой роли
        if ($this->type == 1) {
            $fields['permissions'] = function ($model) {
                $permissions = [];

                foreach (\Yii::$app->authManager->getPermissionsByRole($model->name) as $permission) {
                    $permissions[] = [
                        'name' => $permission->name,
                        'description' => $permission->description,
                    ];
                }

                return $permissions;
            };
        }

        return $fields;
    }
}