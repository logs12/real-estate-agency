<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "advertisement_file".
 *
 * @property int $id Identifier record
 * @property int $advertisement_id Иднтификатор нового объявления квартир
 * @property int $file_id Identifier file
 *
 * @property Advertisement $advertisement
 * @property File $file
 */
class AdvertisementFile extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%advertisement_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['advertisement_id'], 'integer'],

            ['advertisement_id',
                'exist',
                'skipOnError' => true,
                'targetClass' => Advertisement::className(),
                'targetAttribute' => ['advertisement_id' => 'id']
            ],

            [['file_id'], 'integer'],
            [['file_id'], 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'advertisement_id' => 'Advertisement ID',
            'file_id' => 'File ID',
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'advertisement_id' => function ($model) {
                return $this->getIntOrNull($model->advertisement_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getAdvertisement()
    {
        return $this->hasOne(Advertisement::className(), ['id' => 'advertisement_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }
}