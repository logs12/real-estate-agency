<?php

namespace app\models;

use Yii;
use yii\helpers\ArrayHelper;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "{{%news_article_file}}".
 *
 * @property integer $id
 * @property integer $news_article_id
 * @property integer $file_id
 */
class NewsArticleFile extends BaseActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%news_article_file}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['news_article_id', 'integer'],
            ['news_article_id', 'exist', 'skipOnError' => true, 'targetClass' => NewsArticle::className(), 'targetAttribute' => ['news_article_id' => 'id']],

            ['file_id', 'integer'],
            ['file_id', 'exist', 'skipOnError' => true, 'targetClass' => File::className(), 'targetAttribute' => ['file_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => \Yii::t('app/news-article-file', 'id'),
            'news_article_id' => \Yii::t('app/news-article-file', 'news_article_id'),
            'file_id' => \Yii::t('app/news-article-file', 'file_id'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'news_article_id' => function ($model) {
                return $this->getIntOrNull($model->new_article_id);
            },
            'file_id' => function ($model) {
                return $this->getIntOrNull($model->file_id);
            },
        ]);
    }

    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getNewsArticleFile()
    {
        return $this->hasOne(NewsArticleFile::className(), ['id' => 'new_article_id']);
    }
}
