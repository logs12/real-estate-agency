<?php

namespace app\models;
use app\components\BaseActiveRecord;

use app\components\services\CacheService;
use Yii;
use yii\helpers\ArrayHelper;
use app\components\validators\Phone;

/**
 * This is the model class for table "customer_request".
 *
 * @property int $id
 * @property string $name
 * @property int $phone
 * @property int $status_id
 * @property int $created
 * @property int $updated
 * @property int $deleted
 *
 * @property Status $status
 */
class CustomerRequest extends BaseActiveRecord
{

    public $photos;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%customer_request}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [

            ['name', 'string', 'max' => 255],
            ['name', 'required', 'message' => 'Пожалуйста введеите ваше имя'],

            ['phone', Phone::className()],
            ['phone', 'required', 'message' => 'Пожалуйста введите номер телефона'],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/customer_request', 'id'),
            'name' => Yii::t('app/customer_request', 'name'),
            'phone' => Yii::t('app/customer_request', 'phone'),
            'status_id' => Yii::t('app/customer_request', 'status_id'),
            'created' => Yii::t('app/customer_request', 'created'),
            'updated' => Yii::t('app/customer_request', 'updated'),
            'deleted' => Yii::t('app/customer_request', 'deleted'),
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'name',
            'phone',
            'created'
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}
