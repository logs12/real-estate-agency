<?php

namespace app\models;

use Yii;
use app\components\BaseActiveRecord;

/**
 * This is the model class for table "currency".
 *
 * @property int $id Идентификатор записи
 * @property string $name Название валюты
 * @property string $abbreviation Аббревиатура
 * @property int $status_id Идентификатор статуса
 * @property int $created Дата добавления записи
 * @property int $updated Дата изменения записи
 * @property int $deleted Дата удаления записи
 *
 * @property Advertisement[] $apartmentAdvertisements
 * @property Status $status
 */
class Currency extends BaseActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'currency';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'abbreviation', 'status_id'], 'required'],
            [['status_id', 'created', 'updated', 'deleted'], 'integer'],
            [['name', 'abbreviation'], 'string', 'max' => 150],
            [['status_id'], 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'abbreviation' => 'Abbreviation',
            'status_id' => 'Status ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getApartmentAdvertisements()
    {
        return $this->hasMany(Advertisement::className(), ['currency_id' => 'id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }
}
