<?php

namespace app\models;

use app\components\behaviors\UploadFileBehavior;
use app\components\services\CacheService;
use app\components\BaseActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "page".
 *
 * @property int $id Идентификатор записи
 * @property int $status_id Идентификатор статуса
 * @property string $title Заголовок
 * @property string $content Заголовок
 * @property string $created Дата добавления записи
 * @property string $updated Дата изменения записи
 * @property string $deleted Дата удаления записи
 *
 * @property Status $status
 * @property PageFile[] $pageFiles
 */
class Page extends BaseActiveRecord
{
    public $files;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%page}}';
    }

    public function behaviors()
    {
        return ArrayHelper::merge(parent::behaviors(), [
            'uploadFile' => [
                'class' => UploadFileBehavior::className(),
                'relationModelAttributes' => ['files' => [PageFile::className() => ['file_id', 'page_id']]],
            ]
        ]);
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'string'],

            [['content'], 'string'],

            ['files', 'file', 'maxFiles' => 50, 'extensions' => 'png, jpg'],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Заголовок',
            'status_id' => 'Status ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'title',
            'content',
            'files' => function($model) {
                return $model->pageFiles;
            },
            'created',
            'updated',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

    public function getPageFiles()
    {
        return $this->hasMany(File::className(), ['id' => 'file_id'])
            ->viaTable('page_file', ['page_id' => 'id']);
    }

}
