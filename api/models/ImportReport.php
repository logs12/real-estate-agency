<?php

namespace app\models;

use app\components\services\CacheService;
use Yii;
use app\components\BaseActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "import_report".
 *
 * @property int $id Идентификатор записи
 * @property int $file_id Идентификатор файла фотографии
 * @property int $status_id Идентификатор статуса
 * @property string $created Дата добавления записи
 * @property string $updated Дата изменения записи
 * @property string $deleted Дата удаления записи
 *
 * @property Status $status
 */
class ImportReport extends BaseActiveRecord
{

    public $files;

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return '{{%import_report}}';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            ['files', 'file', 'maxFiles' => 50/*, 'extensions' => 'png, jpg, text/html'*/],

            ['status_id', 'integer'],
            ['status_id', 'default', 'value' => CacheService::getStatusByName(static::STATUS_ACTIVE)->id],
            ['status_id', 'exist', 'skipOnError' => true, 'targetClass' => Status::className(), 'targetAttribute' => ['status_id' => 'id']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'status_id' => 'Status ID',
            'created' => 'Created',
            'updated' => 'Updated',
            'deleted' => 'Deleted',
        ];
    }

    public function fields()
    {
        return ArrayHelper::merge(parent::fields(), [
            'id' => function($model) {
                return static::getIntOrNull($model->id);
            },
            'files',
            'created',
            'updated',
        ]);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getFile()
    {
        return $this->hasOne(File::className(), ['id' => 'file_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getStatus()
    {
        return $this->hasOne(Status::className(), ['id' => 'status_id']);
    }

}
