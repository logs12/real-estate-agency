<?php
namespace app\models\search;

use app\models\Advertisement as AdvertisementModel;
use yii\helpers\ArrayHelper;

class AdvertisementSearch extends AdvertisementModel
{
    public function search($params)
    {
        $query = static::find();

        $priceFrom = ArrayHelper::getValue($params, 'condition.price_from');
        $priceTo = ArrayHelper::getValue($params, 'condition.price_to');
        $areaFrom = ArrayHelper::getValue($params, 'condition.area_from');
        $areaTo = ArrayHelper::getValue($params, 'condition.area_to');

        $countRooms = ArrayHelper::getValue($params, 'condition.count_rooms');
        if (is_array($countRooms)) {
            foreach ($countRooms as $key=>$countRoom) {
                if (isset($countRoom)) {
                    if ($countRoom !== '4') {
                        $query->orWhere([static::field('count_rooms') => $countRoom]);
                    } else {
                        $query->orWhere(['>=', static::field('count_rooms'), 4]);
                    }
                }
            }
        }

        if (isset($priceFrom) && isset($priceTo)) {
            $query->andWhere(['between', static::field('price'), $priceFrom, $priceTo]);
        }

        if (isset($priceFrom) && !isset($priceTo)) {
            $query->andWhere(['>', static::field('price'), $priceFrom]);
        }

        if (!isset($priceFrom) && isset($priceTo)) {
            $query->andWhere(['<', static::field('price'), $priceTo]);
        }

        if (isset($areaFrom) && isset($areaTo)) {
            $query->andWhere(['between', static::field('total_area'), $areaFrom, $areaTo]);
        }

        if (isset($areaFrom) && !isset($areaTo)) {
            $query->andWhere(['>', static::field('total_area'), $areaFrom]);
        }

        if (!isset($areaFrom) && isset($areaTo)) {
            $query->andWhere(['<', static::field('total_area'), $areaTo]);
        }

        // Выборка лучших предложений
        if ($isBestOffer = ArrayHelper::getValue($params, 'condition.is_best_offer')) {
            $query->andWhere([static::field('is_best_offer') => $isBestOffer]);
        }

        if ($numberFloor = ArrayHelper::getValue($params, 'condition.number_floor')) {
            $query->andWhere([static::field('number_floor') => $numberFloor]);
        }

        if ($firstName = ArrayHelper::getValue($params, 'condition.country')) {
            $query->andWhere(['like', static::field('country'), $firstName]);
        }

        if ($secondName = ArrayHelper::getValue($params, 'condition.region')) {
            $query->andWhere(['like', static::field('region'), $secondName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.city')) {
            $query->andWhere(['like', static::field('city'), $thirdName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.sub_locality_name')) {
            $query->andWhere(['like', static::field('sub_locality_name'), $thirdName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.address')) {
            $query->andWhere(['like', static::field('address'), $thirdName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.building_type')) {
            $query->andWhere(['like', static::field('building_type'), $thirdName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.building_series')) {
            $query->andWhere(['like', static::field('building_series'), $thirdName]);
        }

        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        return $query;
    }
}