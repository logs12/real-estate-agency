<?php
namespace app\models\search;

use app\models\User as UserModel;
use yii\helpers\ArrayHelper;

class UserSearch extends UserModel
{
    public function search($params)
    {
        /*$query = static::find();

        $query->handleParam($params, 'id', BaseQuery::CONDITION_AND);

        $query->orFilterWhere(['like', static::field('first_name'), ArrayHelper::getValue($params, 'condition.first_name')]);
        $query->orFilterWhere(['like', static::field('second_name'), ArrayHelper::getValue($params, 'condition.second_name')]);
        $query->orFilterWhere(['like', static::field('third_name'), ArrayHelper::getValue($params, 'condition.third_name')]);
        $query->orFilterWhere(['like', static::field('email'), ArrayHelper::getValue($params, 'condition.email')]);
        $query->orFilterWhere(['like', static::field('phone'), ArrayHelper::getValue($params, 'condition.phone')]);

        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        return $query;*/

        $query = static::find();

        if ($firstName = ArrayHelper::getValue($params, 'condition.first_name')) {
            $query->where(['like', static::field('first_name'), $firstName]);
        }

        if ($secondName = ArrayHelper::getValue($params, 'condition.second_name')) {
            $query->orWhere(['like', static::field('second_name'), $secondName]);
        }

        if ($thirdName = ArrayHelper::getValue($params, 'condition.third_name')) {
            $query->orWhere(['like', static::field('third_name'), $thirdName]);
        }

        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        return $query;
    }
}