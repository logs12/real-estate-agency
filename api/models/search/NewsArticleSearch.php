<?php
namespace app\models\search;

use app\components\queries\BaseQuery;
use app\models\NewsArticle as NewsArticleModel;
use yii\helpers\ArrayHelper;

class NewsArticleSearch extends NewsArticleModel
{
    public function search($params)
    {

        $query = static::find();

        if ($title = ArrayHelper::getValue($params, 'condition.title')) {
            $query->where(['like', static::field('state'), $title]);
        }
        if (($order = ArrayHelper::getValue($params, 'order')) && is_array($order)) {
            $query = static::queryOrder($order, $query);
        }

        return $query;
    }
}