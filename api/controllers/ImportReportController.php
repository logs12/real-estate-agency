<?php

namespace app\controllers;

use app\components\controllers\BaseActiveController;
use app\components\exceptions\UserException;
use app\components\services\FileService;
use app\components\services\ImportService;
use app\models\AuthItem;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class ImportReportController extends BaseActiveController
{
    public $modelClass = 'app\models\ImportReport';

    public function init()
    {
        $this->enableCsrfValidation = false;
    }

    public function getQuery()
    {
        $query = parent::getQuery();

        $query->with(['status']);

        return $query;
    }

    public function actionImport()
    {

        $data = \Yii::$app->getRequest()->getBodyParams();
        $file = ArrayHelper::getValue($data, 'file');
        FileService::setGlobalFile($file[0], 'file');

        if (!$file = UploadedFile::getInstanceByName('file')) {
            throw new UserException('Не передан файл с данными');
        }
        return ImportService::createImport($file);
    }


    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case self::ACTION_INDEX:
                $this->checkPermissions([AuthItem::PERMISSION_REPORT_IMPORT_GET]);
                break;
            case self::ACTION_VIEW:
                $this->checkPermissions([
                    AuthItem::PERMISSION_REPORT_IMPORT_GET,
                    AuthItem::PERMISSION_REPORT_IMPORT_VIEW,
                ]);
                break;
            case self::ACTION_UPDATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_REPORT_IMPORT_GET,
                    AuthItem::PERMISSION_REPORT_IMPORT_UPDATE,
                ]);
                break;
            case self::ACTION_CREATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_REPORT_IMPORT_GET,
                    AuthItem::PERMISSION_REPORT_IMPORT_CREATE,
                ]);
                break;
            case self::ACTION_DELETE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_REPORT_IMPORT_GET,
                    AuthItem::PERMISSION_REPORT_IMPORT_DELETE,
                ]);
                break;
            default:
                throw new ErrorException('Не найдено разрешение для действия:' . $action . ' в контроллере ' . get_class($this));
        }
    }
}
