<?php

namespace app\controllers;

use app\components\controllers\BaseActiveController;
use app\components\exceptions\UserException;
use app\components\services\FileService;
use app\components\services\ImportService;
use app\models\AuthItem;
use yii\base\ErrorException;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;

class PageController extends BaseActiveController
{
    public $modelClass = 'app\models\Page';

    public function getQuery()
    {
        $query = parent::getQuery();

        $query->with(['status']);

        return $query;
    }


    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case self::ACTION_INDEX:
                //$this->checkPermissions([AuthItem::PAGE_GET]);
                break;
            case self::ACTION_VIEW:
                /*$this->checkPermissions([
                    AuthItem::PAGE_GET,
                    AuthItem::PAGE_VIEW,
                ]);*/
                break;
            case self::ACTION_UPDATE:
                $this->checkPermissions([
                    AuthItem::PAGE_GET,
                    AuthItem::PAGE_UPDATE,
                ]);
                break;
            case self::ACTION_CREATE:
                $this->checkPermissions([
                    AuthItem::PAGE_GET,
                    AuthItem::PAGE_CREATE,
                ]);
                break;
            case self::ACTION_DELETE:
                $this->checkPermissions([
                    AuthItem::PAGE_GET,
                    AuthItem::PAGE_DELETE,
                ]);
                break;
            default:
                throw new ErrorException('Не найдено разрешение для действия:' . $action . ' в контроллере ' . get_class($this));
        }
    }
}
