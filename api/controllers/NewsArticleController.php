<?php

namespace app\controllers;

use app\components\controllers\BaseActiveController;
use app\models\AuthItem;
use yii\base\UserException;

class NewsArticleController extends BaseActiveController
{
    public $modelClass = 'app\models\NewsArticle';

    public function getQuery()
    {
        $query = parent::getQuery();
        $query->with(['status']);

        return $query;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case self::ACTION_INDEX:
                //$this->checkPermissions([AuthItem::PERMISSION_NEWS_ARTICLE_GET]);
                break;
            case self::ACTION_VIEW:
                //$this->checkPermissions([AuthItem::PERMISSION_NEWS_ARTICLE_GET,]);
                break;
            case self::ACTION_UPDATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_NEWS_ARTICLE_GET,
                    AuthItem::PERMISSION_NEWS_ARTICLE_UPDATE,
                ]);
                break;
            case self::ACTION_CREATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_NEWS_ARTICLE_GET,
                    AuthItem::PERMISSION_NEWS_ARTICLE_CREATE,
                ]);
                break;
            case self::ACTION_DELETE:
                $this->checkPermissions([
                    //AuthItem::PERMISSION_NEWS_ARTICLE_GET,
                    //AuthItem::PERMISSION_NEWS_ARTICLE_DELETE,
                ]);
                break;
            default:
                throw new UserException('Не найдено разрешение для действия:' . $action . ' в контроллере ' . get_class($this));
        }
    }
}
