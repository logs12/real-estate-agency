<?php

namespace app\controllers;

use app\components\controllers\BaseActiveController;
use app\components\services\CacheService;
use app\models\Advertisement;
use app\models\AuthItem;
use yii\base\UserException;

class CommercialEstateAdvertisementController extends BaseActiveController
{
    public $modelClass = 'app\models\Advertisement';

    public function getQuery($discriminator = null)
    {
        $query = parent::getQuery();

        $query->andWhere([Advertisement::field('discriminator') => Advertisement::COMMERCIAL_ESTATE_ADVERTISEMENT_DISCRIMINATOR]);

        $query->with(['status'], ['advertisementFiles']);

        return $query;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        switch ($action) {
            case self::ACTION_INDEX:
                //$this->checkPermissions([AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_GET]);
                break;
            case self::ACTION_VIEW:
                //$this->checkPermissions([AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_GET,]);
                break;
            case self::ACTION_UPDATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_GET,
                    AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_UPDATE,
                ]);
                break;
            case self::ACTION_CREATE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_GET,
                    AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_CREATE,
                ]);
                break;
            case self::ACTION_DELETE:
                $this->checkPermissions([
                    AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_GET,
                    AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_DELETE,
                ]);
                break;
            default:
                throw new UserException('Не найдено разрешение для действия:' . $action . ' в контроллере ' . get_class($this));
        }
    }
}
