<?php

return [
    'id' => 'id',
    'name' => 'Имя',
    'phone' => 'Телефон',
    'status_id' => 'Статус',
    'created' => 'date created',
    'updated' => 'date updated',
    'deleted' => 'date deleted',
];