<?php

return [
    'id' => 'ID',
    'is_manually_added' => 'Добавлено вручную',
    'country' => 'Страна',
    'title' => 'Название объявления',
    'region' => 'Область',
    'city' => 'Город',
    'sub_locality_name' => 'Район',
    'address' => 'Аддрес квартиры',
    'price' => 'Цена',
    'currency' => 'Тип валюты',
    'is_mortgage' => 'Ипотека',
    'count_rooms' => 'Количество комнат',
    'number_floor' => 'Номер этажа',
    'floors_total' => 'Общее количество этажей в доме',
    'is_new_building' => 'Новостройка',
    'is_phone' => 'Телефон',
    'is_internet' => 'Интернет',
    'is_parking' => 'Парковка',
    'is_rubbish_chute' => 'Мусоропровод',
    'is_lift' => 'Лифт',
    'is_gaz' => 'Газ',
    'description' => 'Описание',
    'building_type' => 'Тип дома',
    'building_series' => 'Серия дома',
    'total_area' => 'Общая площадь',
    'lot_area' => 'Площадь участка',
    'kitchen_space' => 'Площадь кухни',
    'living_space' => 'Жилая площадь',
    'photos' => 'Фотографии',
    'status' => 'Статус',
    'created' => 'Дата создания',
    'updated' => 'Дата редактирования',
    'deleted' => 'Дата удаления',
];