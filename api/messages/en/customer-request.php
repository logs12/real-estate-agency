<?php

return [
    'id' => 'id',
    'name' => 'Name',
    'phone' => 'Phone',
    'status_id' => 'identifier status',
    'created' => 'date created',
    'updated' => 'date updated',
    'deleted' => 'date deleted',
];