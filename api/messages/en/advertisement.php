<?php

return [
    'id' => 'ID',
    'is_manually_added' => 'Manually Added',
    'country' => 'Country',
    'title' => 'Title',
    'region' => 'Region',
    'city' => 'City',
    'sub_locality_name' => 'Sub Locality Name',
    'address' => 'Address',
    'price' => 'Price Value',
    'currency' => 'Currency ID',
    'count_rooms' => 'Count Rooms',
    'number_floor' => 'Number Floor',
    'floors_total' => 'Floors Total',
    'is_new_building' => 'Is New Building',
    'is_phone' => 'Is Phone',
    'is_internet' => 'Is Internet',
    'is_parking' => 'Is Parking',
    'is_rubbish_chute' => 'Is Rubbish Chute',
    'is_lift' => 'Is Lift',
    'is_gaz' => 'Is Gaz',
    'description' => 'Description',
    'building_type' => 'Building Type',
    'building_series' => 'Building Series',
    'total_area' => 'Total Area',
    'lot_area' => 'Lot Area',
    'kitchen_space' => 'Kitchen Space',
    'living_space' => 'Living Space',
    'photos' => 'Photos',
    'status' => 'Status',
    'created' => 'Created',
    'updated' => 'Updated',
    'deleted' => 'Deleted',
];