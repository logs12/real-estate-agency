<?php

namespace app\components\behaviors;

use app\components\BaseBehavior;
use app\components\services\FileService;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use app\models\File as FileModel;

class UploadFileBehavior extends BaseBehavior
{
    /**
     * Массив настройки полей для сохранения файлов в связанной модели
     *
     * Event afterSave.
     *
     * ['input name' => ['relation model class name' => [File::$id, $relationModel::$id]]]
     *
     * @var array
     */
    public $relationModelAttributes = [];

    /**
     * Массив настройки полей для сохранения в текущей модели
     *
     * Event beforeSave.
     *
     *
     *
     *
     *       
     *
     * ['input name' => 'attribute name']
     *
     * @var array
     */
    public $ownerModelAttributes = [];

    public function beforeValidate()
    {
        $files = ArrayHelper::merge($this->relationModelAttributes, $this->ownerModelAttributes);

        foreach ($files as $fieldName => $idFieldName) {
            if ($this->owner->{$fieldName}) {

                // если пришел массив файлов
                if (empty($this->owner->{$fieldName}[0])) {
                    FileService::setGlobalFile($this->owner->{$fieldName}, $fieldName);
                    $uploadedFile = UploadedFile::getInstanceByName($fieldName);
                    $this->owner->{$fieldName} = $uploadedFile;
                } else {
                    FileService::setGlobalFiles($this->owner->{$fieldName}, $fieldName);
                    $uploadedFile = UploadedFile::getInstancesByName($fieldName);
                    //$this->getUploadedFile($this->owner->{$fieldName});
                    $this->clearUploadedFiles($this->owner->{$fieldName});
                    $this->owner->{$fieldName} = $uploadedFile;
                }
            }
        }

        parent::beforeValidate();
    }

    /**
     * Deleted old uploaded files
     * @throws \Exception
     * @throws \Throwable
     */
    public function clearUploadedFiles() {
        if (!empty($this->relationModelAttributes)) {
            foreach ($this->relationModelAttributes as $fieldName => $relationAttribute) {
                if ($this->owner->{$fieldName}) {
                    // Выбираем из связанной модели все id загруженных файлов
                    $relationModelAttribute= key($relationAttribute);
                    $fileUploadedIds = $relationModelAttribute::find()
                        ->select(['fileId' => array_values($relationAttribute)[0][0]])
                        ->where([array_values($relationAttribute)[0][1] => $this->owner->id])
                        ->column();

                    $files = FileModel::findAll($fileUploadedIds);
                    foreach ($files as $file) {
                        if($file->delete()) {
                            $path = FileService::getFilesFolder();
                            if (!empty($file->name) && !empty($file->extension)) {
                                unlink($path . $file->name . '.' . $file->extension);
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * Редактирование уже загруженных фалов
     * @param $filesData
     */
    /*public function getUploadedFile($filesData) {

        if (!empty($this->relationModelAttributes)) {
            foreach ($this->relationModelAttributes as $fieldName => $relationAttribute) {
                if ($this->owner->{$fieldName}) {
                    // Выбираем из связанной модели все id загруженных файлов
                    $fileUploadedIds = key($relationAttribute)::find()
                        ->select(['fileId' => array_values($relationAttribute)[0][0]])
                        ->where([array_values($relationAttribute)[0][1] => $this->owner->id])
                        ->column();

                    $fileUploadIds = [];
                    foreach ($filesData as $fileData) {
                        if (array_key_exists('id',$fileData)) {
                            $fileUploadIds[] =  $fileData['id'];
                        }
                    }

                    $deleteIds = array_diff($fileUploadedIds, $fileUploadIds);

                    $files = FileModel::findAll($deleteIds);
                    foreach ($files as $file) {
                        $file->softDelete();
                    }
                }
            }
        }
    }*/

    public function beforeSave()
    {
        if (!empty($this->ownerModelAttributes)) {

            foreach ($this->ownerModelAttributes as $fieldName => $idFieldName) {
                if ($this->owner->{$fieldName}) {
                    $fileModel = FileService::createFile($this->owner->{$fieldName});
                    $this->updateAttribute($fileModel, $idFieldName);
                }
            }
        }

        parent::beforeSave();
    }

    public function afterSave()
    {
        if (!empty($this->relationModelAttributes)) {
            foreach ($this->relationModelAttributes as $fieldName => $relationAttribute) {
                if ($this->owner->{$fieldName}) {
                    $fileModel = FileService::createFile($this->owner->{$fieldName});
                    if (is_array($fileModels = $fileModel)) {
                        foreach ($fileModels as $fileModel) {
                            $this->updateAttribute($fileModel, array_values($relationAttribute)[0], key($relationAttribute));
                        }
                    } else {
                        $this->updateAttribute($fileModel, array_values($relationAttribute)[0], key($relationAttribute));
                    }
                }
            }
        }

        parent::afterSave();
    }

    private function updateAttribute($fileModel, $idFieldName, $relationModelClassName = false)
    {
        if ($relationModelClassName) {

            if (is_array($idFieldName) && !empty($idFieldName[0]) && !empty($idFieldName[1])) {

                $relationModel = new $relationModelClassName;
                $relationModel->{$idFieldName[0]} = $fileModel->id;
                $relationModel->{$idFieldName[1]} = $this->owner->id;
                $relationModel->save();

            } else {
                throw new UserException('Ошибка при сохранении файла. Вы использовали связанную модель для хранения файлов, но не указали необходимые поля в свойстве relationModelAttributes');
            }

        } else {
            $this->owner->{$idFieldName} = $fileModel->id;
        }
    }
}
