<?php
namespace app\components\validators;

use Yii;
use yii\base\InvalidConfigException;
use app\components\BaseActiveRecord;

class ExistValidator extends \yii\validators\ExistValidator
{
    public $discriminator;

    public function validateAttribute($model, $attribute)
    {
        /** @var BaseActiveRecord $targetModel */
        $targetAttribute = $this->targetAttribute === null ? $attribute : $this->targetAttribute;
        $targetModel = $this->targetClass;

        if (is_array($targetAttribute)) {
            if ($this->allowArray) {
                throw new InvalidConfigException('The "targetAttribute" property must be configured as a string.');
            }
            $params = [];
            foreach ($targetAttribute as $k => $v) {
                $params[$targetModel::field($v)] = is_integer($k) ? $model->$v : $model->$k;
            }
        } else {
            $params = [$targetModel::field($targetAttribute) => $model->$attribute];
        }

        if (!$this->allowArray) {
            foreach ($params as $value) {
                if (is_array($value)) {
                    $this->addError($model, $attribute, Yii::t('yii', '{attribute} is invalid.'));
                    return;
                }
            }
        }

        /* @var $targetClass BaseActiveRecord */
        $targetClass = $this->targetClass === null ? get_class($model) : $this->targetClass;
        $query = $this->createQuery($targetClass, $params);

        // Проверка при наличии дискриминатора
        if (!empty($this->discriminator)) {
            $query->andWhere([$targetClass::field('discriminator') => $this->discriminator]);
        }

        if (is_array($model->$attribute)) {
            if ($query->count("DISTINCT [[$targetAttribute]]") != count($model->$attribute)) {
                $this->addError($model, $attribute, $this->message);
            }
        } elseif (!$query->exists()) {
            $this->addError($model, $attribute, $this->message);
        }
    }
}