<?php
namespace app\components\services;

use app\models\Advertisement;
use app\models\AdvertisementFile;
use app\models\File;
use app\models\ImportReport;
use Yii;
use yii\web\UploadedFile;

class ImportService extends BaseService
{

    public static function createImport(UploadedFile $file)
    {

        $path = FileService::getFilesFolder() . '/' . uniqid() . '.xml';

        copy($file->tempName, $path);

        $xml = simplexml_load_file($path);

        $transaction = Yii::$app->db->beginTransaction();
        try {
            foreach ($xml as $key=>$value) {
                if ($key == 'offer') {
                    // Если объявление с таким internal_id еще не было импортировано
                    if (!Advertisement::find()->where(['internal_id' => (string)$value['internal-id']])->exists()) {
                        $advertisement = new Advertisement([
                            'internal_id' => (int)$value['internal-id'],
                            'is_manually_added' => 0,
                            'country' => (string)$value->location->country,
                            'region' => (string)$value->location->region,
                            'city' => (string)$value->location->{'locality-name'},
                            'sub_locality_name' => (string)$value->location->{'sub-locality-name'},
                            'address' => (string)$value->location->address,
                            'price' => (int)$value->price->value,
                            'currency_id' => (string)$value->price->currency == 'RUR' ? 1 : 2,
                            'count_rooms' => (int)$value->rooms,
                            'number_floor' => (int)$value->floor,
                            'floors_total' => (int)$value->{'floors-total'},
                            'is_new_building' => 0,
                            'is_phone' => (int)$value->phone,
                            'is_internet' => (int)$value->internet,
                            'is_parking' => (int)$value->parking,
                            'is_rubbish_chute' => (int)$value->{'rubbish-chute'},
                            'is_lift' => (int)$value->lift,
                            'is_gaz' => 1,
                            'description' => (string)$value->description,
                            'building_type' => (string)$value->{'building-type'},
                            'building_series' => (string)$value->{'building_series'},
                            'total_area' => (int)$value->area->value[0],
                            'lot_area' => (int)$value->{'lot-area'}->value[0],
                            'kitchen_space' => (int)$value->{'kitchen-space'}->value,
                            'living_space' => (int)$value->{'living-space'}->value,
                            'discriminator' => static::getType((string)$value->category),
                            'title' => static::generateTitleAdvertisement(
                                (string)$value->category,
                                (string)$value->location->address,
                                (int)$value->rooms
                            ),
                        ]);

                        $advertisement->saveOrError();

                        foreach ($value->image as $image) {

                            // проверка на доступность файла
                           //if (@fopen((string)$image, "r")) {
                                $file = new File([
                                    'url' => (string)$image
                                ]);
                                $file->saveOrError();

                                $apartmentAdvertisementFile = new AdvertisementFile([
                                    'advertisement_id' => $advertisement->id,
                                    'file_id' => $file->id,
                                ]);
                                $apartmentAdvertisementFile->saveOrError();
                            //}
                        }
                    }
                }
            }

            // Создание и сохранение отчета со статусом pending
            /** @var ImportReport $importReport */
            $importReport = new ImportReport([
                'status_id' => 5,
            ]);

            $importReport->saveOrError();


            $transaction->commit();

            // удаляем обработанный файл
            unlink($path);
        } catch (\Exception $exception) {
            $transaction->rollBack();
            Yii::$app->response->setStatusCode(422, 'Data Validation Failed.');
            return $exception->getMessage();
        }
        return [];
    }

    /**
     * @param null $categoryName
     * @return string
     */
    public static function getType($categoryName = null)
    {
        if (!empty($categoryName)) {
            switch ($categoryName) {
                case 'квартира' : return Advertisement::APARTMENT_ADVERTISEMENT_DISCRIMINATOR;
                case 'дача' : return Advertisement::COTTAGE_HOUSES_ADVERTISEMENT_DISCRIMINATOR;
                case 'участок' : return Advertisement::COTTAGE_HOUSES_ADVERTISEMENT_DISCRIMINATOR;
                case 'коммерческая' : return Advertisement::COMMERCIAL_ESTATE_ADVERTISEMENT_DISCRIMINATOR;
            }
        }
        return Advertisement::APARTMENT_ADVERTISEMENT_DISCRIMINATOR;
    }

    /**
     * @param $category
     * @param $address
     * @param $rooms
     * @return null|string
     */
    public static function generateTitleAdvertisement($category, $address, $rooms)
    {
        switch ($category) {
            case 'дача': {
                return 'дача,'.$address;
            }
            case 'участок': {
                return 'дача,'.$address;
            }
            case 'коммерческая': {
                return 'коммерческая недвежимость,'.$address;
            }
            case 'квартира': {
                switch ($rooms) {
                    case 1: {
                        return 'однокомнатная квартира';
                    }
                    case 2: {
                        return 'двухкомнатная квартира';
                    }
                    case 3: {
                        return 'трехкомнатная квартира';
                    }
                    case 4: {
                        return 'четырехкомнатная квартира';
                    }
                    default: {
                        return 'квартира';
                    }
                }
            }
        }

        return null;
    }
}