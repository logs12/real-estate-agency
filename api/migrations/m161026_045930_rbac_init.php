<?php

use app\components\Migration;
use app\models\AuthItem;
use yii\base\InvalidConfigException;
use yii\rbac\DbManager;

class m161026_045930_rbac_init extends Migration
{
    protected function getAuthManager()
    {
        $authManager = Yii::$app->getAuthManager();
        if (!$authManager instanceof DbManager) {
            throw new InvalidConfigException('You should configure "authManager" component to use database before executing this migration.');
        }
        return $authManager;
    }

    public function init()
    {
        $authManager = $this->getAuthManager();
        $this->db = $authManager->db;

        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->operations = [
            [
                'up' => function () use ($authManager, $tableOptions) {

                    $this->createTable($authManager->ruleTable, [
                        'name' => $this->string(64)->notNull(),
                        'data' => $this->text(),
                        'created_at' => $this->integer(),
                        'updated_at' => $this->integer(),
                        'PRIMARY KEY (name)',
                    ], $tableOptions);
                },
                'down' => function () use ($authManager) {
                    $this->dropTable($authManager->ruleTable);
                },
                'transactional' => false,
            ],
            [
                'up' => function () use ($authManager, $tableOptions) {

                    $this->createTable($authManager->itemTable, [
                        'id' => 'integer NOT NULL AUTO_INCREMENT',
                        'name' => $this->string(64)->notNull(),
                        'type' => $this->integer()->notNull(),
                        'description' => $this->text(),
                        'rule_name' => $this->string(64),
                        'data' => $this->text(),
                        'created_at' => $this->integer(),
                        'updated_at' => $this->integer(),
                        'PRIMARY KEY (name)',
                        'KEY (id)',
                        'FOREIGN KEY (rule_name) REFERENCES ' . $authManager->ruleTable . ' (name)' .
                        ' ON DELETE SET NULL ON UPDATE CASCADE',
                    ], $tableOptions);
                    $this->createIndex('idx-auth_item-type', $authManager->itemTable, 'type');
                },
                'down' => function () use ($authManager) {
                    $this->dropTable($authManager->itemTable);
                },
                'transactional' => false,
            ],
            [
                'up' => function () use ($authManager, $tableOptions) {

                    $this->createTable($authManager->itemChildTable, [
                        'parent' => $this->string(64)->notNull(),
                        'child' => $this->string(64)->notNull(),
                        'PRIMARY KEY (parent, child)',
                        'FOREIGN KEY (parent) REFERENCES ' . $authManager->itemTable . ' (name)' .
                        ' ON DELETE CASCADE ON UPDATE CASCADE',
                        'FOREIGN KEY (child) REFERENCES ' . $authManager->itemTable . ' (name)' .
                        ' ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $tableOptions);
                },
                'down' => function () use ($authManager) {
                    $this->dropTable($authManager->itemChildTable);
                },
                'transactional' => false,
            ],
            [
                'up' => function () use ($authManager, $tableOptions) {

                    $this->createTable($authManager->assignmentTable, [
                        'item_name' => $this->string(64)->notNull(),
                        'user_id' => $this->integer()->unsigned()->comment('ID пользователя'),
                        'created_at' => $this->integer(),
                        'PRIMARY KEY (item_name, user_id)',
                        'FOREIGN KEY (item_name) REFERENCES ' . $authManager->itemTable . ' (name) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT auth_item_2_user_id FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $tableOptions);
                },
                'down' => function () use ($authManager) {
                    $this->dropTable($authManager->assignmentTable);
                },
                'transactional' => false,
            ],
            [
                'up' => function () use ($authManager, $tableOptions) {
                    $permissionsObjects = [];
                    $permissions = [

                        // Настройки
                        AuthItem::PERMISSION_CONFIG_GET => 'Получение настройки',
                        AuthItem::PERMISSION_CONFIG_CREATE => 'Добавление настройки',
                        AuthItem::PERMISSION_CONFIG_UPDATE => 'Изменение настройки',
                        AuthItem::PERMISSION_CONFIG_DELETE => 'Удаление настройки',

                        // Файлы
                        AuthItem::PERMISSION_FILE_GET => 'Получение файлы',
                        AuthItem::PERMISSION_FILE_CREATE => 'Добавление файлы',
                        AuthItem::PERMISSION_FILE_UPDATE => 'Изменение файлы',
                        AuthItem::PERMISSION_FILE_DELETE => 'Удаление файлы',

                        // Разрешения
                        AuthItem::PERMISSION_PERMISSION_GET => 'Получение разрешения',
                        AuthItem::PERMISSION_PERMISSION_CREATE => 'Добавление разрешения',
                        AuthItem::PERMISSION_PERMISSION_UPDATE => 'Изменение разрешения',
                        AuthItem::PERMISSION_PERMISSION_DELETE => 'Удаление разрешения',

                        // Статусы
                        AuthItem::PERMISSION_STATUS_GET => 'Получение статуса',
                        AuthItem::PERMISSION_STATUS_CREATE => 'Добавление статуса',
                        AuthItem::PERMISSION_STATUS_UPDATE => 'Изменение статуса',
                        AuthItem::PERMISSION_STATUS_DELETE => 'Удаление статуса',

                        // Пользователи
                        AuthItem::PERMISSION_USER_GET => 'Получение пользователя',
                        AuthItem::PERMISSION_USER_CREATE => 'Добавление пользователя',
                        AuthItem::PERMISSION_USER_UPDATE => 'Изменение пользователя',
                        AuthItem::PERMISSION_USER_DELETE => 'Удаление пользователя',

                        // Роли
                        AuthItem::PERMISSION_ROLE_GET => 'Получение роли',
                        AuthItem::PERMISSION_ROLE_CREATE => 'Добавление роли',
                        AuthItem::PERMISSION_ROLE_UPDATE => 'Изменение роли',
                        AuthItem::PERMISSION_ROLE_DELETE => 'Удаление роли',

                        // Buiding
                        AuthItem::PERMISSION_BUILDING_GET => 'Get buiding',
                        AuthItem::PERMISSION_BUILDING_CREATE => 'Create buiding',
                        AuthItem::PERMISSION_BUILDING_UPDATE => 'Update buiding',
                        AuthItem::PERMISSION_BUILDING_DELETE => 'Delete buiding',


                        // FLOOR
                        AuthItem::PERMISSION_FLOOR_GET => 'Get buiding',
                        AuthItem::PERMISSION_FLOOR_CREATE => 'Create buiding',
                        AuthItem::PERMISSION_FLOOR_UPDATE => 'Update buiding',
                        AuthItem::PERMISSION_FLOOR_DELETE => 'Delete buiding',

                        // FLAT
                        AuthItem::PERMISSION_FLAT_GET => 'Get buiding',
                        AuthItem::PERMISSION_FLAT_CREATE => 'Create buiding',
                        AuthItem::PERMISSION_FLAT_UPDATE => 'Update buiding',
                        AuthItem::PERMISSION_FLAT_DELETE => 'Delete buiding',

                        // ROOM
                        AuthItem::PERMISSION_ROOM_GET => 'Get buiding',
                        AuthItem::PERMISSION_ROOM_CREATE => 'Create buiding',
                        AuthItem::PERMISSION_ROOM_UPDATE => 'Update buiding',
                        AuthItem::PERMISSION_ROOM_DELETE => 'Delete buiding',

                        // NEWS_ARTICLE
                        AuthItem::PERMISSION_NEWS_ARTICLE_GET => 'Get news article',
                        AuthItem::PERMISSION_NEWS_ARTICLE_CREATE => 'Create news article',
                        AuthItem::PERMISSION_NEWS_ARTICLE_UPDATE => 'Update news article',
                        AuthItem::PERMISSION_NEWS_ARTICLE_DELETE => 'Delete news article',

                        // APARTMENT_ADVERTISEMENT
                        AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_GET => 'Get apartment advertisement article',
                        AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_CREATE => 'Create apartment advertisement article',
                        AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_UPDATE => 'Update apartment advertisement article',
                        AuthItem::PERMISSION_APARTMENT_ADVERTISEMENT_DELETE => 'Delete apartment advertisement article',

                        // HOMESTEAD_ADVERTISEMENT
                        AuthItem::PERMISSION_HOMESTEAD_ADVERTISEMENT_GET => 'Get homestead advertisement article',
                        AuthItem::PERMISSION_HOMESTEAD_ADVERTISEMENT_CREATE => 'Create homestead advertisement article',
                        AuthItem::PERMISSION_HOMESTEAD_ADVERTISEMENT_UPDATE => 'Update homestead advertisement article',
                        AuthItem::PERMISSION_HOMESTEAD_ADVERTISEMENT_DELETE => 'Delete homestead advertisement article',

                        // TRADING_ESTATE_ADVERTISEMENT
                        AuthItem::PERMISSION_TRADING_ESTATE_ADVERTISEMENT_GET => 'Get trading estate advertisement article',
                        AuthItem::PERMISSION_TRADING_ESTATE_ADVERTISEMENT_CREATE => 'Create trading estate advertisement article',
                        AuthItem::PERMISSION_TRADING_ESTATE_ADVERTISEMENT_UPDATE => 'Update trading estate advertisement article',
                        AuthItem::PERMISSION_TRADING_ESTATE_ADVERTISEMENT_DELETE => 'Delete trading estate advertisement article',

                        // CUSTOMER_REQUEST
                        AuthItem::PERMISSION_CUSTOMER_REQUEST_GET => 'Get customer request article',
                        AuthItem::PERMISSION_CUSTOMER_REQUEST_CREATE => 'Create customer request article',
                        AuthItem::PERMISSION_CUSTOMER_REQUEST_UPDATE => 'Update customer request article',
                        AuthItem::PERMISSION_CUSTOMER_REQUEST_DELETE => 'Delete customer request article',

                        // Отчёты импорта
                        AuthItem::PERMISSION_REPORT_IMPORT_GET => 'Получение отчёта импорта',
                        AuthItem::PERMISSION_REPORT_IMPORT_VIEW => 'Просмотр отчёта импорта',
                        AuthItem::PERMISSION_REPORT_IMPORT_CREATE => 'Добавление отчёта импорта',
                        AuthItem::PERMISSION_REPORT_IMPORT_UPDATE => 'Изменение отчёта импорта',
                        AuthItem::PERMISSION_REPORT_IMPORT_DELETE => 'Удаление отчёта импорта',

                        // Страницы
                        AuthItem::PAGE_GET => 'Получение страницы',
                        AuthItem::PAGE_VIEW => 'Просмотр страницы',
                        AuthItem::PAGE_CREATE => 'Добавление страницы',
                        AuthItem::PAGE_UPDATE => 'Изменение страницы',
                        AuthItem::PAGE_DELETE => 'Удаление страницы',

                    ];

                    foreach ($permissions as $permissionName => $permissionDescription) {
                        $permission = $authManager->createPermission($permissionName);
                        $permission->description = $permissionDescription;
                        $authManager->add($permission);
                        $permissionsObjects[$permissionName] = $permission;
                    }

                    $rbac = [
                        AuthItem::ROLE_ROOT => [
                            'description' => 'super admin portal',
                            'permissions' => array_keys($permissions),
                        ],
                        AuthItem::ROLE_ADMIN => [
                            'description' => 'admin portal',
                            'permissions' => array_keys($permissions),
                        ],
                        AuthItem::ROLE_USER => [
                            'description' => 'user',
                            'permissions' => [

                                AuthItem::PERMISSION_FILE_GET,
                                AuthItem::PERMISSION_FILE_CREATE,
                                AuthItem::PERMISSION_FILE_UPDATE,
                                AuthItem::PERMISSION_FILE_DELETE,
                                AuthItem::PERMISSION_STATUS_GET,
                                AuthItem::PERMISSION_STATUS_CREATE,
                                AuthItem::PERMISSION_STATUS_UPDATE,
                                AuthItem::PERMISSION_STATUS_DELETE,

                                // RBAC
                                AuthItem::PERMISSION_ROLE_GET,
                                AuthItem::PERMISSION_ROLE_CREATE,
                                AuthItem::PERMISSION_ROLE_UPDATE,
                                AuthItem::PERMISSION_ROLE_DELETE,
                                AuthItem::PERMISSION_PERMISSION_GET,
                                AuthItem::PERMISSION_PERMISSION_CREATE,
                                AuthItem::PERMISSION_PERMISSION_UPDATE,
                                AuthItem::PERMISSION_PERMISSION_DELETE,
                                AuthItem::PERMISSION_USER_GET,
                                AuthItem::PERMISSION_USER_CREATE,
                                AuthItem::PERMISSION_USER_UPDATE,
                                AuthItem::PERMISSION_USER_DELETE,

                                // BUILDING
                                AuthItem::PERMISSION_BUILDING_GET,
                                AuthItem::PERMISSION_BUILDING_CREATE,
                                AuthItem::PERMISSION_BUILDING_UPDATE,
                                AuthItem::PERMISSION_BUILDING_DELETE,

                                // FLOOR
                                AuthItem::PERMISSION_FLOOR_GET,
                                AuthItem::PERMISSION_FLOOR_CREATE,
                                AuthItem::PERMISSION_FLOOR_UPDATE,
                                AuthItem::PERMISSION_FLOOR_DELETE,

                                // FLAT
                                AuthItem::PERMISSION_FLAT_GET,
                                AuthItem::PERMISSION_FLAT_CREATE,
                                AuthItem::PERMISSION_FLAT_UPDATE,
                                AuthItem::PERMISSION_FLAT_DELETE,

                                // ROOM
                                AuthItem::PERMISSION_ROOM_GET,
                                AuthItem::PERMISSION_ROOM_CREATE,
                                AuthItem::PERMISSION_ROOM_UPDATE,
                                AuthItem::PERMISSION_ROOM_DELETE,

                                // NEWS ARTICLE
                                AuthItem::PERMISSION_NEWS_ARTICLE_GET,
                                AuthItem::PERMISSION_NEWS_ARTICLE_CREATE,
                                AuthItem::PERMISSION_NEWS_ARTICLE_UPDATE,
                                AuthItem::PERMISSION_NEWS_ARTICLE_DELETE,
                            ],
                        ],
                    ];
                    foreach ($rbac as $roleName => $roleData) {
                        $role = $authManager->createRole($roleName);
                        $role->description = $roleData['description'];
                        $authManager->add($role);
                        foreach ($roleData['permissions'] as $permissionName) {
                            $permission = $permissionsObjects[$permissionName];
                            $authManager->addChild($role, $permission);
                        }
                    }
                },
                'down' => function () use ($authManager) {
                    Yii::$app->db->createCommand('delete from ' . $authManager->assignmentTable);
                    Yii::$app->db->createCommand('delete from ' . $authManager->itemChildTable);
                    Yii::$app->db->createCommand('delete from ' . $authManager->itemTable);
                    Yii::$app->db->createCommand('delete from ' . $authManager->ruleTable);
                },
                'transactional' => true,
            ],
        ];
    }
}
