<?php

use app\components\Migration;
use yii\db\Schema;
use app\models\Status;
use app\models\Type;

class m161025_194027_init extends Migration
{
    public function init()
    {
        $this->operations = [

            [
                'up' => function () {
                    $this->createTable('{{%status}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'title' => $this->string(255)->notNull()->comment('Название'),
                        'name' => $this->string(255)->notNull()->comment('Системное название'),
                        'UNIQUE KEY `status-name--unique` (`name`)',
                    ], $this->getTableOptions('Все статусы системы'));
                },
                'down' => function () {
                    $this->dropTable('status');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'name' => $this->string(255)->notNull()->comment('Название файла в нашей файловой системе'),
                        'filename' => $this->string(255)->notNull()->comment('Оригинальное название файла'),
                        'url' => $this->string()->comment('Ссылка на файл если он загружается с внешнего источника'),
                        'extension' => $this->string(10)->notNull()->comment('Расширение файла'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'created_by' => $this->integer()->unsigned()->comment('Идентификатор пользователя создавшего запись'),
                        'updated_by' => $this->integer()->unsigned()->comment('Идентификатор пользователя редактировавшего запись'),
                        'CONSTRAINT file_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Файлы'));
                },
                'down' => function () {
                    $this->dropTable('file');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%user}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'first_name' => $this->string(150)->notNull()->comment('Имя'),
                        'second_name' => $this->string(150)->comment('Фамилия'),
                        'third_name' => $this->string(150)->comment('Отчество'),
                        'email' => $this->string(255)->notNull()->comment('Электропочта'),
                        'type_id' => $this->integer()->unsigned()->notNull()->comment('Тип пользователя'),
                        'file_id' => $this->integer()->unsigned()->comment('Идентификатор файла фотографии'),
                        'phone' => $this->string(25)->comment('Телефон'),
                        'password_hash' => $this->string(60)->notNull()->comment('Хэш пароля'),
                        'auth_key' => $this->string(32)->comment('Ключ подтверждения для cookie аутентификации'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        //'role_name' => $this->string(64)->comment('Наименование роли'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT user_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT user_2_file FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Users'));
                },
                'down' => function () {
                    $this->dropTable('{{%user}}');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->createTable('{{%type}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'title' => $this->string(255)->notNull()->comment('Type name'),
                        'name' => $this->string(255)->notNull()->comment('System name'),
                        'UNIQUE KEY `type-name--unique` (`name`)',
                    ], $this->getTableOptions('All type system'));
                },
                'down' => function () {
                    $this->dropTable('type');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%currency}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'name' => $this->string(150)->notNull()->comment('Название валюты'),
                        'abbreviation' => $this->string(150)->notNull()->comment('Аббревиатура'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT currency_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Справочник валют'));
                },
                'down' => function () {
                    $this->dropTable('{{%currency}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%news_article}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'title' => $this->string(150)->notNull()->comment('Название новости'),
                        'content' => $this->text()->comment('Содержимое новости'),
                        'file_id' => $this->integer()->unsigned()->comment('Идентификатор файла фотографии'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT news_article_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT news_article_2_file FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('News articles'));
                },
                'down' => function () {
                    $this->dropTable('{{%news_article}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%news_article_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier record'),
                        'news_article_id' => $this->integer()->unsigned()->comment('Identifier new article'),
                        'file_id' => $this->integer()->unsigned()->comment('Identifier file'),
                        'CONSTRAINT news_article_file_2_new_article FOREIGN KEY (news_article_id) REFERENCES news_article (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT news_article_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('News articles images'));
                },
                'down' => function () {
                    $this->dropTable('{{%news_article_file}}');
                },
                'transactional' => false,
            ],
            
            [
                'up' => function () {
                    $this->createTable('{{%customer_request}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'name' => $this->string(25)->comment('Имя'),
                        'phone' => $this->string(25)->comment('Телефон'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT customer_request_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Customer request'));
                },
                'down' => function () {
                    $this->dropTable('{{%customer_request}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%advertisement}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier'),
                        'internal_id' => $this->bigInteger()->unsigned()->comment('Идентификатор объявления из импортируемого xml документа'),
                        'is_manually_added' => $this->boolean()->defaultValue(1)->comment('true-добавлено вручную, false - импортированно'),
                        'title' => $this->string(155)->notNull()->comment('Название объявления'),
                        'country' => $this->string(155)->comment('Страна'),
                        'region' => $this->string(155)->comment('Область'),
                        'city' => $this->string(155)->comment('Город'),
                        'sub_locality_name' => $this->string(155)->comment('Район'),
                        'address' => $this->string(155)->comment('Аддрес квартиры'),
                        'price' => $this->bigInteger()->unsigned()->notNull() ->comment('Цена'),
                        'currency_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор валюты'),
                        'is_mortgage' => $this->boolean()->defaultValue(1)->comment('true-есть ипотека, false - нет ипотеки'),
                        'count_rooms' => $this->integer()->unsigned()->notNull()->comment('Количество комнат'),
                        'number_floor' => $this->integer()->unsigned()->notNull()->comment('Номер этажа'),
                        'floors_total' => $this->integer()->unsigned()->notNull()->comment('Общее количество этажей в доме'),
                        'is_new_building' => $this->boolean()->defaultValue(0)->comment('true - новостройка, false - вторичка'),
                        'is_phone' => $this->boolean()->defaultValue(0)->comment('true-есть телефон, false - нет телефона'),
                        'is_internet' => $this->boolean()->defaultValue(0)->comment('true-есть интернет, false - нет интеренета'),
                        'is_parking' => $this->boolean()->defaultValue(0)->comment('true-есть паркинг, false - нет паркинга'),
                        'is_rubbish_chute' => $this->boolean()->defaultValue(0)->comment('true-есть мусоропровод, false - нет мусоропровода'),
                        'is_lift' => $this->boolean()->defaultValue(0)->comment('true-есть лифт, false - нет лифт'),
                        'is_gaz' => $this->boolean()->defaultValue(0)->comment('true-есть газ, false - нет газа'),
                        'description' => $this->text()->comment('Описание'),
                        'building_type' => $this->string(155)->comment('Тип дома'),
                        'building_series' => $this->string(155)->comment('Серия дома'),
                        'total_area' => $this->integer()->unsigned()->null()->comment('Общая площадь'),
                        'kitchen_space' => $this->integer()->unsigned()->null()->comment('Площадь кухни'),
                        'living_space' => $this->integer()->unsigned()->null()->comment('Жилая площадь'),
                        'lot_area' => $this->integer()->unsigned()->null()->comment('Площадь участка'),
                        'file_id' => $this->integer()->unsigned()->comment('Идентификатор файла фотографии'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Identifier status'),
                        'discriminator' => $this->string(32)->notNull()->comment('Дискриминатор сущности'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT advertisement_2_currency FOREIGN KEY (currency_id) REFERENCES currency (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT advertisement_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT advertisement_2_file FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Объявления'));
                },
                'down' => function () {
                    $this->dropTable('{{%advertisement}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%advertisement_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier record'),
                        'advertisement_id' => $this->integer()->unsigned()->comment('Иднтификатор нового объявления'),
                        'file_id' => $this->integer()->unsigned()->comment('Identifier file'),
                        'CONSTRAINT advertisement_file_2_advertisement FOREIGN KEY (advertisement_id) REFERENCES advertisement (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT advertisement_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Связи многие ко многим для фотографий объявлений'));
                },
                'down' => function () {
                    $this->dropTable('{{%advertisement_file}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%import_report}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT report_import_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Отчёты импорта'));
                },
                'down' => function () {
                    $this->dropTable('{{%import_report}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%page}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Идентификатор записи'),
                        'title' => $this->string(155)->notNull()->comment('Название объявления'),
                        'content' => $this->text()->comment('Описание'),
                        'file_id' => $this->integer()->unsigned()->comment('Идентификатор файла'),
                        'status_id' => $this->integer()->unsigned()->notNull()->comment('Идентификатор статуса'),
                        'created' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP COMMENT "Дата добавления записи"',
                        'updated' => Schema::TYPE_TIMESTAMP . ' DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT "Дата изменения записи"',
                        'deleted' => Schema::TYPE_TIMESTAMP . ' NULL COMMENT "Дата удаления записи"',
                        'CONSTRAINT page_2_status FOREIGN KEY (status_id) REFERENCES status (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Страницы'));
                },
                'down' => function () {
                    $this->dropTable('{{%page}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->createTable('{{%page_file}}', [
                        'id' => $this->primaryKey()->unsigned()->comment('Identifier record'),
                        'page_id' => $this->integer()->unsigned()->comment('Иднтификатор нового объявления'),
                        'file_id' => $this->integer()->unsigned()->comment('Identifier file'),
                        'CONSTRAINT page_file_2_page FOREIGN KEY (page_id) REFERENCES page (id) ON DELETE CASCADE ON UPDATE CASCADE',
                        'CONSTRAINT page_file_2_file_id FOREIGN KEY (file_id) REFERENCES file (id) ON DELETE CASCADE ON UPDATE CASCADE',
                    ], $this->getTableOptions('Связи многие ко многим для страниц'));
                },
                'down' => function () {
                    $this->dropTable('{{%page_file}}');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {

                    // Инициализация статусов
                    $status = new Status([
                        'title' => 'Активный',
                        'name' => 'active',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Неактивный',
                        'name' => 'inactive',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Удалён',
                        'name' => 'deleted',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Не применён',
                        'name' => 'not_applied',
                    ]);
                    $status->saveOrError();

                    $status = new Status([
                        'title' => 'Применён',
                        'name' => 'applied',
                    ]);
                    $status->saveOrError();


/*                    $type = new Type([
                        'title' => 'Квартира',
                        'name' => 'apartment',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Дома, дачи, коттеджи, участки',
                        'name' => 'cottage_houses_land',
                    ]);
                    $type->saveOrError();

                    $type = new Type([
                        'title' => 'Коммерческая недвижимость',
                        'name' => 'commercial_estate',
                    ]);
                    $type->saveOrError();*/

                },
                'down' => function () {
                    Status::deleteAll();
                    Type::deleteAll();
                },
                'transactional' => true,
            ],
        ];
    }
}

