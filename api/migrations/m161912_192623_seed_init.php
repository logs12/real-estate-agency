<?php

use app\models\WebService;
use app\components\Migration;
use app\models\User;
use app\models\Status;
use app\models\AuthItem;
use app\models\Building;
use app\models\Floor;
use app\models\Flat;
use app\models\Room;
use app\models\Page;
use app\models\NewsArticle;
use app\models\CustomerRequest;
use app\models\Advertisement;
use app\models\Currency;

class m161912_192623_seed_init extends Migration
{
    public function init()
    {
        $this->operations = [
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `user` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `file` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `status` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            [
                'up' => function () {
                    $this->execute('ALTER TABLE `news_article` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `customer_request` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `advertisement` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `import_report` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],

            [
                'up' => function () {
                    $this->execute('ALTER TABLE `page` AUTO_INCREMENT=1');
                },
                'transactional' => false,
            ],
            
            [
                'up' => function () {

                    $rub = new Currency([
                        'name' => 'Российский рубль',
                        'abbreviation' => 'руб.',
                        'status_id' => 1,
                    ]);

                    $rub->saveOrError();

                    $usd = new Currency([
                        'name' => 'Доллар',
                        'abbreviation' => '$',
                        'status_id' => 1,
                    ]);

                    $usd->saveOrError();

                    $page = new Page([
                        'title' => 'Главная'
                    ]);
                    $page->saveOrError();

                    $page = new Page([
                        'title' => 'О компании'
                    ]);
                    $page->saveOrError();

                    $page = new Page([
                        'title' => 'Работа в агентве'
                    ]);
                    $page->saveOrError();

                    for($i = 1; $i<= 90; $i++) {
                        // Пользователи
                        $authManager = Yii::$app->authManager;
                        $rootRole = $authManager->getRole(AuthItem::ROLE_ROOT);
                        $adminRole = $authManager->getRole(AuthItem::ROLE_ADMIN);
                        $userRole = $authManager->getRole(AuthItem::ROLE_USER);

                        if ($i === 1) {
                            $userName = $rootRole->description;
                        } elseif ($i === 2) {
                            $userName = $adminRole->description;
                        } elseif ($i === 3) {
                            $userName = $adminRole->description;
                        } else {
                            $userName = $userRole->description . ' #' . $i;
                        }

                        $phoneSuffix = str_pad($i, 4, "0", STR_PAD_LEFT);

                        $user = new User([
                            'first_name' => $userName,
                            'second_name' => 'Second name #' . $i,
                            'third_name' => 'Third name #' . $i,
                            'email' => 'user-'. $i .'@mail.ru',
                            'phone' => '+7908123' . $phoneSuffix,
                            'password' => '12345',
                            'type_id' => 0,
                        ]);
                        $user->saveOrError();

                        if ($i === 1) {
                            $authManager->assign($rootRole, $user->id);
                        } elseif ($i === 2) {
                            $authManager->assign($adminRole, $user->id);
                        } elseif ($i === 3) {
                            $authManager->assign($adminRole, $user->id);
                        } else {
                            $authManager->assign($userRole, $user->id);
                        }

                        $newArticle = new NewsArticle([
                            'title' => 'Title #' . $i,
                            'content' => '<p>Content #' . $i .'ContentContentContentContentContentContentContentContentContentContentContentContentContentContentContent
   ContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContentContent<p>',
                        ]);

                        $newArticle->saveOrError();

                        $customerRequest = new CustomerRequest([
                            'name' => 'Name #' . $i,
                            'phone' => '+7908123' . $phoneSuffix,
                        ]);

                        $customerRequest->saveOrError();

                      /*  $apartmentAdvertisement = new ApartmentAdvertisement([
                            'is_manually_added' => 1,
                            'country' => 'Россия',
                            'region' => 'Нижегородская область',
                            'city' => 'Нижний Новгород',
                            'sub_locality_name' => 'Автозаводский',
                            'address' =>  'Богдановича, 25',
                            'price' => 1000000,
                            'currency_id' => 1,
                            'is_mortgage' => 0,
                            'count_rooms' => 1,
                            'number_floor' => 2,
                            'floors_total' => 9,
                            'is_new_building' => 0,
                            'is_phone' => 1,
                            'is_internet' => 1,
                            'is_parking' => 1,
                            'is_rubbish_chute' => 1,
                            'is_lift' => 1,
                            'is_gaz' => 1,
                            'description' => $i . 'Описание Описание Описание Описание Описание Описание Описание Описание Описание ',
                            'building_type' => 'Кирпичный',
                            'building_series' => 'Нар. стройка',
                            'total_area' => 40,
                            'kitchen_space' => 20,
                            'living_space' => 37,
                            'file_id' => null,
                            'status_id' => 1,
                            'created' => time(),
                        ]);

                        $apartmentAdvertisement->saveOrError();*/
                    }
                },
                'down' => function () {
                    User::deleteAll();

                    Yii::$app->authManager->removeAllAssignments();
                }
            ]
        ];
    }
}
