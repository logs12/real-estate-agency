<?php

use app\components\Migration;

class m170810_074945_add_column_is_best_offer_in_advertisement extends Migration
{
    public function init()
    {
        $this->operations = [

            [
                'up' => function () {
                    $this->addColumn(
                        '{{%advertisement}}',
                            'is_best_offer',
                        $this->boolean()
                            ->defaultValue(0)
                            ->comment('1 - является лучшим предложением, 0 - не является')
                    );
                },
                'down' => function () {
                    $this->dropColumn('{{%advertisement}}', 'is_best_offer');
                },
                'transactional' => false,
            ],
        ];
    }
}
