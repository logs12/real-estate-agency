<?php

return [
    'environment' => 'production',
    'applicationDomains' => [],
    'authTimeOut' => 3600 * 24 * 30, // 30 дней
    'fiasToken' => '4312f0dfbbb9ed60280bc768e0e19fba6dec378b',
    'mainEmail' => 'admin@lada.com',
];
