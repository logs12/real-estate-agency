import './style.scss';

import React, { Component } from 'react';

import AdvertisementListItems from '../../components/AdvertisementListItems';

import Filter from '../../components/Filter';

import {
    NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE,
    NEW_APARTMENT_ADVERTISEMENT_URL_REQUEST,
} from "../../../common/constants";


export default class NewApartmentAdvertisementsFrontendPage extends Component{

    render()  {

        return (
            <div className="new-apartment-advertisements-frontend-page">
                <div className="content-grid">
                    {/*<h4 className="apartment-advertisements-frontend-page__caption-page">Список квартир</h4>*/}

                    <Filter
                        entityName='ApartmentAdvertisement'
                        urlGetRequest={NEW_APARTMENT_ADVERTISEMENT_URL_REQUEST}
                        gridScheme={[4,3]}
                        filterFields={{
                            'address': {
                                typeField: 'text',
                                label: 'Район, улица',
                            },
                            'number_floor': {
                                typeField: 'text',
                                label: 'Номер этажа',
                            },
                            'count_rooms': {
                                typeField: 'toggleListField',
                                label: 'комнат',
                                defaultValue: [
                                    {
                                        'label': '1',
                                        'value': '1',
                                    },
                                    {
                                        'label': '2',
                                        'value': '2',
                                    },
                                    {
                                        'label': '3',
                                        'value': '3',
                                    },
                                    {
                                        'label': '4+',
                                        'value': '4',
                                    },
                                ]
                            },
                            'price_from': {
                                typeField: 'text',
                                label: 'Цена от (руб.)',
                            },
                            'price_to': {
                                typeField: 'text',
                                label: 'Цена до (руб.)',
                            },
                            'area_from': {
                                typeField: 'text',
                                label: 'Площадь от (м²)',
                            },
                            'area_to': {
                                typeField: 'text',
                                label: 'Площадь до (м²)',
                            },
                        }}
                    />
                    
                    <AdvertisementListItems
                        entityName='ApartmentAdvertisement'
                        urlGetRequest={NEW_APARTMENT_ADVERTISEMENT_URL_REQUEST}
                        urlContentView = {NEW_APARTMENT_ADVERTISEMENT_FRONTEND_VIEW_ROUTE}
                    />
                </div>
            </div>
        )
    }
};