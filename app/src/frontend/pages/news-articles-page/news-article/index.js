import './style.scss';

import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import { BasePageView } from '../../../../common/decorators/BasePageView';

class NewsArticleViewFrontendPage extends Component {


    render() {

        const { model } = this.props;

        return (
            <div className="news-article-view-page">
                <div className="content-grid">
                    {(!isEmpty(model)) ?
                        <div>
                            <h5>{model.title}</h5>
                            <div className="news-article-view-page__content"
                                 dangerouslySetInnerHTML={{__html: model.content}}>
                            </div>
                        </div>
                        : null}
                </div>
            </div>
        )
    }
}

export default BasePageView(
    NewsArticleViewFrontendPage,
    'NewsArticle',
);