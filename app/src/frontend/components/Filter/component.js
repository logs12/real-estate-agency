import React, { createElement } from 'react';

import TextField from 'react-mdl/lib/Textfield';
import classNames from 'classnames';
import CustomToggleListField from '../../../common/components/custom-toggle-list-field';

const FilterComponent = (props) => {

    const { gridScheme, filterFields, handleChange } = props;

    let startIndex = 0,
        endIndex = 0;

    return (
        <div className="filter-container">
            {gridScheme.map((countCollFields, index) => {
                startIndex = index > 0 ? startIndex + 12/gridScheme[index-1] : index;
                endIndex = index > 0 ? startIndex + 12/countCollFields : 12/countCollFields;
                return FilterComponent.renderFilterFields(index, startIndex, endIndex, countCollFields, filterFields, handleChange);
            })}
        </div>
    )
};

/**
 * @param index
 * @param startIndex
 * @param endIndex
 * @param countCollFields
 * @param filterFields
 * @param handleChange
 * @returns {XML}
 */
FilterComponent.renderFilterFields = (index, startIndex, endIndex, countCollFields, filterFields, handleChange) => {
    let countObject = 0,
        fieldElements = [],
        collClassName = classNames(
            'mdl-cell',
            `mdl-cell--${countCollFields}-col`,
            'mdl-cell--12-col-tablet',
            'mdl-cell--12-col-phone'
        );

    for (let filterField in filterFields) {
        if (filterFields.hasOwnProperty(filterField)) {
            if (countObject >= startIndex && countObject < endIndex) {
                switch (filterFields[filterField].typeField) {
                    case 'toggleListField': {
                        fieldElements.push(
                            <div className={collClassName}
                                key={countObject}
                            >
                                <CustomToggleListField
                                    handleChange={(value)=>{handleChange(value, filterField)}}
                                    labelCommon={filterFields[filterField].label}
                                    defaultValue={filterFields[filterField].defaultValue}
                                />
                            </div>
                        );
                        break;
                    }
                    default: {
                        fieldElements.push(
                            <div className={collClassName}
                                 key={countObject}
                            >
                                <TextField
                                    onChange={(event)=>{handleChange(event.target.value, filterField)}}
                                    label={filterFields[filterField].label}
                                />
                            </div>
                        );
                        break;
                    }
                }
            }
        }
        countObject++;
    }

    return <div className="mdl-grid"
            key={index}
        >
        {fieldElements}
    </div>
};

export default FilterComponent;