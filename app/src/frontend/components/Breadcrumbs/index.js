import React, { Component, createElement } from 'react';
import PropTypes from 'prop-types';
import ItemBreadcrumbHoc from './container';
import classNames from 'classnames';

/**
 * Виджет можно использовать независимо от формы, но в таком случае необходимо явно прописывать название
 * entityName - название сущности со значением которой будет работатьь виджет
 * в глобальном state в свойстве reducerName, которому принадлежат данные
 <InputText
 name = {'password'}
 label = {'Пароль'}
 typeField = {'textField'},
 initFocused
 />
 */
export default class ItemBreadcrumb extends Component {

    static defaultProps = {
        entityName: null,
    };

    /**
     * Инициализируем контроль типов свойств
     * @type {{name: *, label: *, label: *}}
     */
    static propTypes = {
        entityName: PropTypes.string,
    };

    render () {
        const {
            entityName,
        } = this.props;

        return  createElement(ItemBreadcrumbHoc(
            entityName,
        ))
    }
};