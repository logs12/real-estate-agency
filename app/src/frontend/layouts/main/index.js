import './style.scss';

import React, { Component } from 'react';
import Header from '../header';
import Footer from '../footer';
import Breadcrumbs from '../../../common/widgets/breadcrumbs-widget';

import ProgressBarWidget from '../../../common/widgets/progress-bar-widget';

export default class Main extends Component {
    render() {

        const {routes, params, location} = this.props;
        return (
            <div className="frontend-layout-main">

                <main className="wrapper mdl-layout__content">
                    <ProgressBarWidget />
                    <Header/>
                    <Breadcrumbs
                        className='breadcrumbs'
                        routes={routes}
                        params={params}
                        location={location}
                        separatorRenderer={() => <li><i className="fa fa-angle-right" aria-hidden="true"></i></li>}
                    />
                    <div className="wrapper-page-content ">
                        {this.props.children}
                    </div>
                    <div className="mdl-layout-spacer"></div>

                    <Footer/>
                </main>
            </div>
        )
    }
}