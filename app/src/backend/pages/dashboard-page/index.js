import './style.scss';
import React, { Component } from 'react';



export default class DashboardPage extends Component{

    render()  {

        return (
            <div className="dashboard-page" >
                <div className="mdl-grid">
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp mdl-color--orange-800">
                            <div className="dashboard-page__card-content">
                                <div className="dashboard-page__card-icon">
                                    <i className="mdl-color-text--white material-icons" role="presentation">description</i>
                                </div>
                                <div className="dashboard-page__card-number-container">
                                    <div className="dashboard-page__card-number">50</div>
                                    <div className="">заявок</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp mdl-color--deep-purple-500">
                            <div className="dashboard-page__card-content">
                                <div className="dashboard-page__card-icon">
                                    <i className="mdl-color-text--white material-icons" role="presentation">people</i>
                                </div>
                                <div className="dashboard-page__card-number-container">
                                    <div className="dashboard-page__card-number">100</div>
                                    <div className="">Объявлений по новостройкам</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp mdl-color--blue-A400">
                            <div className="dashboard-page__card-content">
                                <div className="dashboard-page__card-icon">
                                    <i className="mdl-color-text--white material-icons" role="presentation">book</i>
                                </div>
                                <div className="dashboard-page__card-number-container">
                                    <div className="dashboard-page__card-number">100</div>
                                    <div className="">Объявлений по вторичной недвижимости</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp mdl-color--pink-700">
                            <div className="dashboard-page__card-content">
                                <div className="dashboard-page__card-icon">
                                    <i className="mdl-color-text--white material-icons" role="presentation">people</i>
                                </div>
                                <div className="dashboard-page__card-number-container">
                                    <div className="dashboard-page__card-number">10</div>
                                    <div className="">пользователей</div>
                                </div>
                            </div>
                        </div>

                    </div>
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp">
                            <div className="mdl-card__title">
                                <h2 className="mdl-card__title-text">О портале</h2>
                            </div>
                            <div className="mdl-card__supporting-text">
                                Наш новый гид по порталу расскажет и покажет вам как
                                пользоваться системой с наибольшим удобством и эффективностью
                            </div>
                            <div className="mdl-card__actions mdl-card--border">
                                <a href="#about" className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                    Перейти
                                </a>
                            </div>
                        </div>

                    </div>
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp">
                            <div className="mdl-card__title">
                                <h2 className="mdl-card__title-text">Гид по порталу</h2>
                            </div>
                            <div className="mdl-card__supporting-text">
                                Наш новый гид по порталу расскажет и покажет вам как
                                пользоваться системой с наибольшим удобством и эффективностью
                            </div>
                            <div className="mdl-card__actions mdl-card--border">
                                <a href="#guide" className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                    Начать
                                </a>
                            </div>
                        </div>

                    </div>
                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp">
                            <div className="mdl-card__title">
                                <h2 className="mdl-card__title-text">Что нового</h2>
                            </div>
                            <div className="mdl-card__supporting-text">
                                Описание обновлений и нововведений
                            </div>
                            <div className="mdl-card__actions mdl-card--border">
                                <a href="#release-notes" className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                    Посмотреть
                                </a>
                            </div>
                        </div>

                    </div>

                    <div className="mdl-cell mdl-cell--3-col">

                        <div className="dashboard-page__card dashboard-page__card--narrow mdl-card mdl-shadow--2dp">
                            <div className="mdl-card__title">
                                <h2 className="mdl-card__title-text">Гид по порталу</h2>
                            </div>
                            <div className="mdl-card__supporting-text">
                                Наш новый гид по порталу расскажет и покажет вам как
                                пользоваться системой с наибольшим удобством и эффективностью
                            </div>
                            <div className="mdl-card__actions mdl-card--border">
                                <a href="#about" className="mdl-button mdl-button--colored mdl-js-button mdl-js-ripple-effect">
                                    Начать
                                </a>
                            </div>
                        </div>
                    </div>

                </div>
            </div>
        )
    }
};