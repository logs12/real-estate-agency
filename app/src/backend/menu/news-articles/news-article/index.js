import './style.scss';

import React, { Component } from 'react';
import isEmpty from 'lodash/isEmpty';
import Article from '../../../../common/widgets/article-widget/component';
import BackToListButton from '../../../components/BackToListButton';
import { BasePageView } from '../../../../common/decorators/BasePageView';

import {
    NEWS_ARTICLE_ROUTE,
} from "../../../../common/constants";

class NewsArticleView extends Component {

    render() {

        const { model, pushToRouter } = this.props;

        return (
            <div className="news-article-view-page">
                <Article>

                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={NEWS_ARTICLE_ROUTE} />

                    {(!isEmpty(model)) ?
                        <div>
                            <p><b>Title:</b> {model.title}</p>
                            <p><b>Content:</b> {model.content}</p>
                        </div>
                    : null}

                </Article>
            </div>
        )
    }
}

export default BasePageView(NewsArticleView, 'NewsArticle');