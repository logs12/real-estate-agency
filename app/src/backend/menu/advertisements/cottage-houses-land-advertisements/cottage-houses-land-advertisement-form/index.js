import React, { Component } from 'react';
import { connect } from 'react-redux';
import { bindActionCreators } from "redux";
import { push } from 'react-router-redux';
import find from 'lodash/find';
import FormWidget from '../../../../../common/widgets/form-widget';
import TextInputWidget from '../../../../../common/widgets/text-input-widget';
import ButtonLoadingWidget from '../../../../../common/widgets/button-loading-widget';
import FileInputMultipleWidget from '../../../../../common/widgets/file-input-multiple-widget';
import BackToListButton from '../../../../components/BackToListButton';
import TextDraftWysiwygWidget from '../../../../../common/widgets/text-draft-wysiwyg-widget';

import Checkbox from 'react-mdl/lib/Checkbox';

import { baseGetAction } from '../../../../../common/actions/BaseAction';
import {
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST,
    COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE,
    COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE,
} from "../../../../../common/constants";

const initModel = {
    'photos': [],
    'is_manually_added': 1,
    'is_best_offer': 0,
    'currency_id': 1,
    'country': null,
    'title': null,
    'region': null,
    'city': null,
    'sub_locality_name': null,
    'address': null,
    'price': null,
    'floors_total': null,
    'building_type': null,
    'total_area': null,
    'kitchen_space': null,
    'living_space': null,
    'lot_area': null,
    'description': null,
    'discriminator': 'cottage_houses_land',
};

const mapStateToProps = (state, ownProps) => ({
    cottageHousesLandAdvertisementId: Number(ownProps.params.id),
    cottageHousesLandAdvertisements: state.CottageHousesLandAdvertisement.collection,
    cottageHousesLandAdvertisement: find(state.CottageHousesLandAdvertisement.collection, {id: Number(ownProps.params.id)})
        ? find(state.CottageHousesLandAdvertisement.collection, {id: Number(ownProps.params.id)})
        : initModel,
});

const mapDispatchToProps = (dispatch) => ({
    cottageHousesLandAdvertisementsGetAction: bindActionCreators(baseGetAction, dispatch),
    pushToRouter: bindActionCreators(push, dispatch),
});

@connect(mapStateToProps, mapDispatchToProps)

export default class CottageHousesLandAdvertisementsForm extends Component {

    /**
     * Name action for formWidget
     * @type {string}
     */
    formAction = 'baseCreateAction';

    componentWillMount() {

        // If entity update
        if (this.props.cottageHousesLandAdvertisementId) {
            if (!this.props.cottageHousesLandAdvertisements.length) {
                this.props.cottageHousesLandAdvertisementsGetAction(
                    COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST(),
                    this.props.cottageHousesLandAdvertisementId,
                    'CottageHousesLandAdvertisement',
                );
            }
            this.formAction = 'baseUpdateAction';
        }
    }

    render () {

        const { cottageHousesLandAdvertisement, cottageHousesLandAdvertisementId, pushToRouter } = this.props;

        return (
            <div className="cottage-houses-land-advertisement-form">
                <div className="mdl-card mdl-shadow--2dp wide">
                    <BackToListButton pushToRouter={pushToRouter} routeBackToList={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE} />
                    <FormWidget
                        actionName={this.formAction}
                        entityName="CottageHousesLandAdvertisement"
                        initModel={cottageHousesLandAdvertisement}
                        url={COTTAGE_HOUSES_LAND_ADVERTISEMENT_URL_REQUEST(cottageHousesLandAdvertisementId)}>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name = 'title'
                                    label = 'Название объявления'
                                    initFocused
                                />
                            </div>
                        </div>
                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'country'
                                    label = 'Страна'
                                    initFocused
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'region'
                                    label = 'Область'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'city'
                                    label = 'Город'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'sub_locality_name'
                                    label = 'Район'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'address'
                                    label = 'Адрес'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'price'
                                    label = 'Цена'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextInputWidget
                                    name='is_best_offer'
                                    label='Лучшее предложение (показывать ли объявление в виджете "лучшее предложение")'
                                    typeField='checkbox'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name = 'floors_total'
                                    label = 'Общее количество этажей в доме'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--6-col">
                                <TextInputWidget
                                    name='building_type'
                                    label='Тип дома'
                                />
                            </div>
                        </div>


                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='total_area'
                                    label='Общая площадь'
                                />
                            </div>
                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='kitchen_space'
                                    label='Площадь кухни'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='living_space'
                                    label='Жилая площадь'
                                />
                            </div>

                            <div className="mdl-cell mdl-cell--4-col">
                                <TextInputWidget
                                    name='lot_area'
                                    label='Площадь участка'
                                />
                            </div>
                        </div>

                        <div className="mdl-grid">
                            <div className="mdl-cell mdl-cell--12-col">
                                <TextDraftWysiwygWidget
                                    name = 'description'
                                    label = 'Описание'
                                />
                            </div>
                        </div>


                        <div className="mdl-grid">

                            <div className="mdl-cell mdl-cell--12-col">
                                <FileInputMultipleWidget
                                    type = 'file'
                                    name = 'photos'
                                    title = 'Выберите изображение для загрузки'
                                />
                            </div>
                        </div>

                        <div className="form-widget__actions mdl-card__actions mdl-card--border mdl-grid">
                            <ButtonLoadingWidget
                                label="Сохранить"
                                type="saveAndBackToList"
                                backToListUrl={COTTAGE_HOUSES_LAND_ADVERTISEMENTS_ROUTE}
                            />
                            { !cottageHousesLandAdvertisementId ?
                                <ButtonLoadingWidget
                                    label="Сохранить & Создать новую"
                                    type="saveAndMore"
                                    backToListUrl={COTTAGE_HOUSES_LAND_ADVERTISEMENT_CREATE_ROUTE}
                                />
                                : null
                            }

                        </div>
                    </FormWidget>
                </div>
            </div>
        );
    }
}