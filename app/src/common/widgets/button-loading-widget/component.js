import './style.scss';

import React, { Component } from "react";
import PropTypes from "prop-types";
import classNames from "classnames";

import {
    FORM_WIDGET_REQUEST,
} from '../../constants';

import Button from 'react-mdl/lib/Button';

export default class ButtonLoadingComponent extends Component {

    static propTypes = {
        className: PropTypes.string,
        label: PropTypes.string,
        disabled: PropTypes.bool,
        stateElement: PropTypes.object,
    };

    /**
     * Метод выключения кнопки, кнопка должна быть выключенна, если нет изменений у сущности и если происходит
     * процесс коннекта с сервером
     * @returns {*}
     */
    renderDisabled() {
        if (this.props.disabled /*|| this.props.buttonState == FORM_WIDGET_REQUEST*/) {
            return 'disabled';
        }
    }

    render() {

        const { className, handleClickButton, label, stateElement } = this.props;

        const classes = classNames('widget-button-loading',className);

        return (
            <div className={classes}>
                <Button disabled={this.renderDisabled()}
                        onClick={handleClickButton}
                >
                    {label}
                </Button>
                <div className="widget-button-loading__state-container">
                    {stateElement}
                </div>
            </div>
        )
    }
}